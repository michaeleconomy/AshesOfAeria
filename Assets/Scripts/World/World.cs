﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class World : MonoBehaviour {
    public static World instance;

    public GameObject movePointer; //TODO - Move this.

    public static bool inEditMode;

	private void Awake () {
        instance = this;
        inEditMode = SceneManager.GetActiveScene().name.Contains("Edit");
    }


    public List<ManagedPrefab> RootLevelManagedPrefabs() {
        var managedPrefabs = new List<ManagedPrefab>();
        for (var i = 0; i < transform.childCount; i++) {
            var child = transform.GetChild(i);
            var prefab = child.GetComponent<ManagedPrefab>();
            if (prefab == null) {
                continue;
            }
            managedPrefabs.Add(prefab);
        }
        return managedPrefabs;
    }
}

