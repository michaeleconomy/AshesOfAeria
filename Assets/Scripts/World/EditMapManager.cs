﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EditMapManager : MapManager {
    public static EditMapManager instance;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        SwitchToEdit(mapData.startMap, true);
    }

    public IEnumerable<string> MapList() {
        return mapData.MapNames();
    }

    public void New(string mapName) {
        if (mapData.Contains(mapName)) {
            throw new Exception("map name already exists: " + mapName);
        }
        var newMap = new MapSave {
            name = mapName,
            prefabs = new List<MapSavePrefab>(),
            tileLayout = new int[0],
            xMin = 0,
            xSize = 0,
            yMin = 0,
            ySize = 0
        };
        mapData.Add(newMap);
        LoadBaseMap(mapName, true);
        CameraController.Center();
    }

    public void Delete(string mapName) {
        mapData.Remove(mapName);
        if (currentMap == mapName) {
            LoadBaseMap(mapData.startMap, true);
            CameraController.Center();
        }
    }

    public void MakeStartMap() {
        mapData.startMap = currentMap;
    }

    public void RenameMap(string oldName, string newName) {
        mapData.Rename(oldName, newName);
        DialogData.instance.RenameMap(oldName, newName);
        if (currentMap == oldName) {
            currentMap = newName;
        }
    }

    public MapData SaveBaseMaps() {
        SaveCurrentBaseMap();
        return mapData;
    }

    public void SwitchToEdit(string mapName, bool skipSave = false) {
        if (!skipSave) {
            SaveCurrentBaseMap();
        }
        LoadBaseMap(mapName, false);
        if (Application.isPlaying) {
            CameraController.Center();
            MapEditor.instance.SelectCursor();
        }
    }

    public void ResetCurrentMap() {
        LoadBaseMap(currentMap, false);
    }

    public void SaveCurrentBaseMap() {
        var mapSave = new MapSave() {
            name = currentMap
        };

        var bounds = GetOverallBounds();
        mapSave.SetBounds(bounds);

        var mapSize = bounds.size.x * bounds.size.y;
        var tileLayoutSize = tilemaps.Count * mapSize;

        mapSave.tileLayout = new int[tileLayoutSize];

        for (var tilemapIndex = 0; tilemapIndex < tilemaps.Count; tilemapIndex++) {
            var tilemap = tilemaps[tilemapIndex];
            var tiles = tilemap.GetTilesBlock(bounds);
            var tilemapOffset = tilemapIndex * mapSize;
            for (var tileIndex = 0; tileIndex < mapSize; tileIndex++) {
                var tile = tiles[tileIndex];
                var tileId = tileManager.GetIdForTile(tile);
                mapSave.tileLayout[tilemapOffset + tileIndex] = tileId;
            }
        }

        mapSave.prefabs = new List<MapSavePrefab>();
        foreach (var prefab in world.RootLevelManagedPrefabs()) {
            var savedPrefab = MapSavePrefab.Save(prefab);
            mapSave.prefabs.Add(savedPrefab);
        }

        mapData.Add(mapSave);
    }

    public BoundsInt GetOverallBounds() {
        var xMin = 0;
        var xMax = 0;
        var yMin = 0;
        var yMax = 0;

        foreach (var tilemap in tilemaps) {
            tilemap.CompressBounds();
            var bounds = tilemap.cellBounds;
            if (bounds.xMin < xMin) {
                xMin = bounds.xMin;
            }
            if (bounds.xMax > xMax) {
                xMax = bounds.xMax;
            }
            if (bounds.yMin < yMin) {
                yMin = bounds.yMin;
            }
            if (bounds.yMax > yMax) {
                yMax = bounds.yMax;
            }
        }

        return new BoundsInt(xMin, yMin, 0, xMax - xMin, yMax - yMin, 1);
    }
}
