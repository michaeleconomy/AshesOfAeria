﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MapData : ScriptableObject {

    public List<MapSave> maps = new List<MapSave>();
    public string startMap = "Vasho";

    private Dictionary<string, MapSave> mapsByName = new Dictionary<string, MapSave>();


    private void OnEnable() {
        foreach (var map in maps) {
            mapsByName[map.name] = map;
        }
    }

    public MapSave GetMap(string mapName) {
        MapSave map;
        if (!mapsByName.TryGetValue(mapName, out map)) {
            throw new Exception("map not found: " + mapName);
        }
        return map;
    }

    public bool Contains(string mapName) {
        return mapsByName.ContainsKey(mapName);
    }

    public IEnumerable<string> MapNames() {
        return mapsByName.Keys;
    }

    public void Add(MapSave map) {
        MapSave existing;
        if (mapsByName.TryGetValue(map.name, out existing)) {
            maps.Remove(existing);
        }
        maps.Add(map);
        mapsByName[map.name] = map;
        if (startMap == null) {
            startMap = map.name;
        }
    }

    public void Remove(string mapName) {
        if (mapName == startMap) {
            throw new Exception("can't remove start map");
        }
        var map = GetMap(mapName);
        maps.Remove(map);
        mapsByName.Remove(mapName);
    }

    public void Rename(string oldName, string newName) {
        if (Contains(newName)) {
            throw new Exception("name already exists: " + newName);
        }
        var map = GetMap(oldName);
        mapsByName.Remove(oldName);
        map.name = newName;
        mapsByName[newName] = map;
        if (startMap == oldName) {
            startMap = newName;
        }
    }

    public void Reload(List<MapSave> mapSaves, string _startMap) {
        Clear();
        foreach (var map in mapSaves) {
            Add(map);
        }
        startMap = _startMap;
    }

    private void Clear() {
        maps.Clear();
        mapsByName.Clear();
        startMap = null;
    }
}
