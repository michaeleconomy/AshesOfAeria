﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
[ExecuteInEditMode]
public class DialogData : ScriptableObject {
    public static DialogData instance;

    public List<DialogForMapSpeaker> dialogs = new List<DialogForMapSpeaker>();
    private readonly Dictionary<string, DialogForMapSpeaker> dialogsIndexed =
            new Dictionary<string, DialogForMapSpeaker>();

    private void OnEnable() {
        instance = this;
        Reindex();
    }

    private void Reindex() {
        dialogsIndexed.Clear();
        foreach (var dialog in dialogs) {
            dialogsIndexed[KeyFor(dialog.mapName, dialog.speakerName)] = dialog;
        }
    }

    public List<DialogNode> GetDialog(string map, string speaker) {
        DialogForMapSpeaker dialog;
        var key = KeyFor(map, speaker);
        if (!dialogsIndexed.TryGetValue(key, out dialog)) {
            return null;
        }
        return dialog.dialogNodes;
    }

    private string KeyFor(string map, string speaker) {
        return map + "-" + speaker;
    }

    public IEnumerable<string> SpeakersInArea(string map) {
        return dialogs.Where((d) => {
            return d.mapName == map;
        }).Select((d) => {
            return d.speakerName;
        });
    }

    public void Add(string map, string speaker, List<DialogNode> nodes) {
        DialogForMapSpeaker dialog;
        var key = KeyFor(map, speaker);
        if (!dialogsIndexed.TryGetValue(key, out dialog)) {
            dialog = new DialogForMapSpeaker {
                mapName = map,
                speakerName = speaker
            };
            dialogs.Add(dialog);
            dialogsIndexed[key] = dialog;
        }
        dialog.dialogNodes = nodes;
    }

    public void Remove(string map, string speaker) {
        DialogForMapSpeaker dialog;
        var key = KeyFor(map, speaker);
        if (!dialogsIndexed.TryGetValue(key, out dialog)) {
            return;
        }
        dialogs.Remove(dialog);
        dialogsIndexed.Remove(key);
    }

    public void RenameMap(string oldName, string newName) {
        foreach (var dialog in dialogs) {
            if (dialog.mapName == oldName) {
                dialog.mapName = newName;
            }
        }
        Reindex();
    }


    public void RenameSpeaker(string map, string oldName, string newName) {
        DialogForMapSpeaker dialog;
        if (!dialogsIndexed.TryGetValue(KeyFor(map, oldName), out dialog)) {
            return;
        }
        dialog.speakerName = newName;
    }

    public void Reload(List<DialogForMapSpeaker> _dialogs) {
        dialogs = _dialogs;
        Reindex();
    }
}
