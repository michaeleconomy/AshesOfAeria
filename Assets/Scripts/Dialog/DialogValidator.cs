﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class DialogValidator {
    private List<DialogNode> dialog;
    readonly List<string> errors = new List<string>();
    Stack<string> nodesToCheck = new Stack<string>();
    HashSet<string> nodesVisited = new HashSet<string>();

    private string nodeName;
    private int currentLineNumber;
    private Stack<bool> indent = new Stack<bool>();
    private string[] currentNodeLines;
    private bool spelling;

    private static HashSet<string> wordList;


    public static List<string> Validate(List<DialogNode> dialog, bool spelling = false) {
        var validator = new DialogValidator {
            dialog = dialog,
            spelling = spelling
        };
        validator.Validate();

        return validator.errors;
    }

    private void Validate() {
        nodesToCheck.Push(DialogRunner.startNodeName);
        while(nodesToCheck.Count > 0) {
            nodeName = nodesToCheck.Pop();
            if (nodesVisited.Contains(nodeName)) {
                continue;
            }
            nodesVisited.Add(nodeName);
            CheckNode();
        }
        currentLineNumber = 0;
        foreach (var node in dialog) {
            nodeName = node.name;
            if (!nodesVisited.Contains(nodeName)) {
                Error("node unreachable");
                if (nodeName == "end") {
                    Error("'end' is not a valid node name");
                }
            }
        }
    }

    Regex dialogLineRegexp = new Regex(@"^(1|2|\w+\:)?(?<dialog>.+)$");

    private void CheckNode() {
        if (!GetNode()) {
            errors.Add("no node named '" + nodeName + "' was found");
        }

        while (currentLineNumber < currentNodeLines.Length) {
            var line = currentNodeLines[currentLineNumber];

            if (!StartsWithSpaces(currentLineNumber, indent.Count)) {
                if (indent.Pop()) {
                    if (HasElse(currentLineNumber, indent.Count)) {
                        indent.Push(false);
                        currentLineNumber++;
                        if(!StartsWithSpaces(currentLineNumber, indent.Count)) {
                            errors.Add(nodeName + currentLineNumber +
                                       " - else contains no content");
                        }
                    }
                }
                continue;
            }

            line = line.Substring(indent.Count);
            if (line.Length == 0) {
                //skip blank lines
                currentLineNumber++;
                continue;
            }
            switch (line[0]) {
                case '-':
                    DashLine(line.Substring(1));
                    break;
                case '@':
                    var nextNodeName = line.Substring(1).Trim();
                    if (nextNodeName != "end") {
                        nodesToCheck.Push(nextNodeName);
                    }
                    currentLineNumber++;
                    if (StartsWithSpaces(currentLineNumber, indent.Count)) {
                        errors.Add(nodeName + currentLineNumber +
                                  "unreachable");
                    }
                    break;
                case '?':
                    ChoiceLine();
                    break;
                default:
                    var words = dialogLineRegexp.Match(line).Groups["dialog"].Value;
                    Spelling(words);
                    currentLineNumber++;
                    break;
            }
        }
    }

    void ChoiceLine() {
        var spacesInBlock = indent.Count + 1;
        var choice =
            currentNodeLines[currentLineNumber].Substring(spacesInBlock).Trim();
        var conditional = ChoiceConditional(choice);
        if (conditional != null) {
            Conditional(conditional);
            choice = choice.Substring(conditional.Length);
        }
        Spelling(choice);
        currentLineNumber++;
        indent.Push(false);
    }

    Regex wordsRegex = new Regex("\\w+");

    private void Spelling(string words) {
        if (!spelling) {
            return;
        }
        if (wordList == null) {
            wordList = new HashSet<string>();
            var textFile = Resources.Load<TextAsset>("wordlist");
            var file = textFile.text;
            var allWords = file.Split('\n');
            foreach(var word in allWords) {
                wordList.Add(word.ToLower());
            }
        }
        var matches = wordsRegex.Matches(words);
        for (int i = 0; i < matches.Count; i++) {
            var word = matches[i].Value;
            if (!wordList.Contains(word.ToLower())) {
                Error(word + " - may be spelled incorrectly");
            }
        }
    }

    Regex andRegex = new Regex(@"^(?<left>(?:[^\(\)]|(?<paren>\()|(?<-paren>\)))+)(?(paren)(?!))&&(?<right>.+)$");
    Regex orRegex = new Regex(@"^^(?<left>(?:[^\(\)]|(?<paren>\()|(?<-paren>\)))+)(?(paren)(?!))\|\|(?<right>.+)$");
    Regex methodCallRegex = new Regex(@"^(?<method>[\w]+)\(((?<param>[^,()]+)(?:(?:\,?)(?<param>[^,()]+))*)?\)$");
    Regex variableRegex = new Regex(@"^\w+$");
    private void Conditional(string conditional) {

        conditional = conditional.Trim();
        if (WrappedInParens(conditional)) {
            Conditional(conditional.Substring(1, conditional.Length - 2));
            return;
        }

        var andMatch = andRegex.Match(conditional);
        if (andMatch.Success) {
            Conditional(andMatch.Groups["left"].Value);
            Conditional(andMatch.Groups["right"].Value);
            return;
        }

        var orMatch = orRegex.Match(conditional);
        if (orMatch.Success) {
            Conditional(orMatch.Groups["left"].Value);
            Conditional(orMatch.Groups["right"].Value);
            return;
        }

        if (conditional.StartsWith("!")) {
            Conditional(conditional.Substring(1));
            return;
        }

        var variableMatch = variableRegex.Match(conditional);
        if (variableMatch.Success) {
            return;
        }

        var methodMatch = methodCallRegex.Match(conditional);
        if (methodMatch.Success) {
            var parameters = new List<string>();
            var captures = methodMatch.Groups["param"].Captures;
            for (var i = 0; i < captures.Count; i++) {
                var capture = captures[i];
                parameters.Add(capture.Value.Trim());
            }
            ConditionalMethod(methodMatch.Groups["method"].Value, parameters);
            return;
        }

        Error("unrecognized format: " + conditional);
    }


    private bool WrappedInParens(string conditional) {
        if (!conditional.StartsWith("(") || !conditional.EndsWith(")")) {
            return false;
        }

        var parenLevel = 0;
        for (var i = 1; i < conditional.Length - 1; i++) {
            switch (conditional[i]) {
                case '(':
                    parenLevel++;
                    break;
                case ')':
                    if (parenLevel <= 0) {
                        return false;
                    }
                    parenLevel--;
                    break;
            }
        }
        return parenLevel == 0;
    }

    Dictionary<string, ArgumentType[]> conditionalMethods = new Dictionary<string, ArgumentType[]> {
        {"hasItem", new ArgumentType[] {ArgumentType.simpleString}},
        {"takeItem", new ArgumentType[] {ArgumentType.simpleString}},
        {"hasQuest", new ArgumentType[] {ArgumentType.questId}},
        {"completedQuest", new ArgumentType[] {ArgumentType.questId}},
        {"onQuest", new ArgumentType[] {ArgumentType.questId}},
        {"met", new ArgumentType[] {}},
        {"onObjective", new ArgumentType[] {ArgumentType.questId, ArgumentType.integer}},
        {"completedObjective", new ArgumentType[] {ArgumentType.simpleString}}
    };

    private void ConditionalMethod(string method, List<string> parameters) {
        ArgumentType[] arguments;
        if (!conditionalMethods.TryGetValue(method, out arguments)) {
            Error("unknown conditional method: " + method);
            return;
        }

        if (arguments.Length != parameters.Count) {
            Error("incorrect number of parameters for method: " + method +
                  ", " + arguments.Length + " expected, seen:" + parameters.Count);
            return;
        }
        for (var i = 0; i < arguments.Length; i++) {
            Argument(parameters[i], arguments[i]);
        }
    }

    private void Argument(string argument, ArgumentType argumentType) {
        switch (argumentType) {
            case ArgumentType.integer:
                int result;
                if (!int.TryParse(argument, out result)) {
                    Error("integer parse error: " + argument);
                }
                break;
            case ArgumentType.map:
                if (!EditMapManager.instance.mapData.Contains(argument)) {
                    Error("map not found: " + argument);
                }
                break;
            case ArgumentType.prefabName:
                if (World.instance.FindChildByName<ManagedPrefab>(argument) == null) {
                    Error("prefab not found: " + argument);
                }
                break;
            case ArgumentType.questId:
                if (!QuestManager.instance.questsById.ContainsKey(argument)) {
                    Error("questId not found: " + argument);
                }
                break;
            case ArgumentType.simpleString:
                break;
        }
    }

    private string ChoiceConditional(string choice) {
        if (!choice.StartsWith("(")) {
            return null;
        }
        var parens = 1;
        for (var i = 1; i < choice.Length; i++) {
            switch (choice[i]) {
                case '(':
                    parens++;
                    break;
                case ')':
                    parens--;
                    if (parens == 0) {
                        return choice.Substring(0, i + 1);
                    }
                    break;
            }
        }
        return null;
    }

    public bool StartsWithSpaces(int lineNum, int spaces) {
        if (lineNum >= currentNodeLines.Length) {
            return false;
        }
        var line = currentNodeLines[lineNum];
        if (line.Length < spaces) {
            return false;
        }
        for (int i = 0; i < spaces; i++) {
            if (line[i] != ' ') {
                return false;
            }
        }
        return true;
    }

    private void DashLine(string line) {
        line = line.Trim();
        if (line.StartsWith("if")) {
            var conditional = line.Substring(2);
            Conditional(conditional);

            indent.Push(true);
            currentLineNumber++;
            if (!StartsWithSpaces(currentLineNumber, indent.Count)) {
                Error("if has no block");
            }
        }
        else {
            Command(line);
            currentLineNumber++;
        }
    }

    public enum ArgumentType {
        prefabName,
        integer,
        questId,
        simpleString,
        map
    }

    Dictionary<string, ArgumentType[]> commands = new Dictionary<string, ArgumentType[]> {
        {"meet", null},
        {"startQuest", new ArgumentType[] {ArgumentType.questId}},
        {"completeObjective", new ArgumentType[] {ArgumentType.simpleString}},
        {"abandonQuest", new ArgumentType[] {ArgumentType.questId}},
        {"giveItem", new ArgumentType[] {ArgumentType.simpleString}},
        {"hide", new ArgumentType[] {ArgumentType.prefabName}},
        {"unhide", new ArgumentType[] {ArgumentType.prefabName}},
        {"set", new ArgumentType[] {ArgumentType.simpleString}},
        {"leave", null},
        {"attack", null},
        {"join", null},
        {"trigger", new ArgumentType[] {ArgumentType.simpleString, ArgumentType.integer}},
        {"travel", new ArgumentType[] {ArgumentType.map, ArgumentType.simpleString, ArgumentType.integer}}
    };

    private void Command(string commandString) {
        var commandPieces = commandString.Split(' ');
        var command = commandPieces[0];
        ArgumentType[] arguments;
        if (!commands.TryGetValue(command, out arguments)) {
            Error("command not found: " + command);
            return;
        }
        if (arguments == null) {
            if (commandPieces.Length > 1) {
                Error("command " + command + " takes 0 arguments");
            }
            return;
        }
        if (arguments.Length != commandPieces.Length - 1) {
            Error("command " + command + " - incorrect number of arguments. " +
                  arguments.Length + " expected, " + (commandPieces.Length - 1) + 
                 " sent");
            return;
        }
        for (int i = 0; i < arguments.Length; i++) {
            Argument(commandPieces[i + 1], arguments[i]);
        }
    }

    private void Error(string error) {
        errors.Add(nodeName + ":" + currentLineNumber + " - " + error);
    }

    private bool HasElse(int lineNum, int spaces) {
        if (!StartsWithSpaces(lineNum, spaces)) {
            return false;
        }
        var line = currentNodeLines[lineNum];
        if (line.Length < spaces + 5) {
            return false;
        }
        if (line[spaces] != '-') {
            return false;
        }
        return line.Substring(spaces + 1).Trim() == "else";
    }

    private int EndOfBlock(int startLine, int spacesInBlock) {
        var endOfBlock = startLine;

        while (StartsWithSpaces(endOfBlock, spacesInBlock)) {
            endOfBlock++;
        }
        if (endOfBlock == startLine) {
            throw new Exception("if has nothing in it");
        }
        return endOfBlock;
    }

    private bool GetNode() {
        DialogNode node = null;
        foreach (var n in dialog) {
            if (n.name == nodeName) {
                node = n;
                break;
            }
        } 
        if (node == null) {
            return false;
        }
        currentLineNumber = 0;
        indent.Clear();
        currentNodeLines = node.dialog.Split('\n');

        return true;
    }
}
