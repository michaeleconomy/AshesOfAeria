﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class DialogTemplates {

    private static List<DialogTemplate> templates = new List<DialogTemplate>{
        new DialogTemplate{
            name = "intro",
            nodes = new List<DialogTemplate.NodeTemplate>{
                new DialogTemplate.NodeTemplate {
                    name = "start",
                    dialog =
                        "-if met()\n" +
                        " @base\n" +
                        "1Hey there, my name is Maru!\n" +
                        "2Oh hello Maru, nice to meet you\n" +
                        "2My name is #name.\n" +
                        "1Nice to meet you #name\n" +
                        "@base"
                },
                new DialogTemplate.NodeTemplate {
                    name = "base",
                    dialog = ""
                }
            }
        },
        new DialogTemplate{
            name = "dialogtree",
            nodes = new List<DialogTemplate.NodeTemplate>{
                new DialogTemplate.NodeTemplate {
                    name = "base",
                    dialog =
                        "?How are you doing #name?\n" +
                        " @howAreYou\n" +
                        "?Whats new?\n" +
                        " @new\n" +
                        "?Whats happening in #map?\n" +
                        " @town\n" +
                        "?How about this weather?\n" +
                        " @weather\n" +
                        "?See you later #name\n" +
                        " See you later #name"
                },

                new DialogTemplate.NodeTemplate {
                    name = "howAreYou",
                    dialog =
                        "1How are you doing #name?\n" +
                        "2OH I'm doing fine. You?\n" +
                        "1Doing OK, All things considered\n" +
                        "@base"
                },

                new DialogTemplate.NodeTemplate {
                    name = "new",
                    dialog =
                        "1Whats new?\n" +
                        "2OH nothing much, a little bit of this and that.\n" +
                        "@base"
                },

                new DialogTemplate.NodeTemplate {
                    name = "town",
                    dialog =
                        "1Whats happening in #map?\n" +
                        "2TODO\n" +
                        "@base"
                },

                new DialogTemplate.NodeTemplate {
                    name = "weather",
                    dialog =
                        "1How about this weather?\n" +
                        "2Oh, this sure is some weather\n" +
                        "@base"
                }
            }
        },

        new DialogTemplate{
            name = "quest",
            prompts = new List<string>{
                "#questId"
            },
            nodes = new List<DialogTemplate.NodeTemplate>{
                new DialogTemplate.NodeTemplate {
                    name = "base",
                    dialog =
                        "-if !hasQuest(#questId)\n" +
                        " @quest\n" +
                        "-if onQuest(#questId)\n" +
                        " 2Hows the QUEST TO DO THE THING coming?\n" +
                        " 1It's coming, I'll get to it shortly\n" +
                        " 2Thats great.\n" +
                        " 2I can't wait to hear about it!\n" +
                        "-if completedQuest(#questId)\n" +
                        " 2Thanks so much for taking care of the QUESTTHING\n" +
                        " 1It's really no problem.\n" +
                        " 1I'm happy to help out.\n"
                },
                new DialogTemplate.NodeTemplate {
                    name = "quest",
                    dialog =
                        "2Have you heard about the QUESTTHING?\n" +
                        "1No, what's up?\n" +
                        "2DESCRIBE PROBLEM\n" +
                        "1That sounds pretty troublsome\n" +
                        "2Tell me about it\n" +
                        "1What can I do to help?\n" +
                        "2You think you could DOTHISTHING?\n" +
                        "1I can try.\n" +
                        "2That would be fantastic\n" +
                        "-startQuest(#questId)\n"
                }
            }
        },

        new DialogTemplate{
            name = "travel",
            nodes = new List<DialogTemplate.NodeTemplate>{
                new DialogTemplate.NodeTemplate {
                    name = "start",
                    dialog =
                        "?Enter\n" +
                        " -travel MAP destination 0\n" +
                        "?Cancel\n"
                }
            }
        }
    };

    class DialogTemplate {
        public string name;
        public List<NodeTemplate> nodes;
        public List<string> prompts;

        public class NodeTemplate {
            public string name;
            public string dialog;
        }
    }

    public static List<string> TemplateNames() {
        return templates.Select((t) => t.name).ToList();
    }

    public static List<string> PromptsFor(int templateId) {
        var template = templates[templateId];
        return template.prompts;
    }

    public static List<DialogNode> ApplyTemplate(int templateId, List<DialogNode> nodes, string speakerName, List<string> inputs) {
        var template = templates[templateId];

        var map = EditMapManager.instance.currentMap;
        foreach (var nodeTemplate in template.nodes) {
            var existing = nodes.First((n) => n.name == nodeTemplate.name);
            var dialog = nodeTemplate.dialog.
                                     Replace("#name", speakerName).
                                     Replace("#map", map);
            if (template.prompts != null) {
                for (var i = 0; i < template.prompts.Count; i++) {
                    dialog = dialog.Replace(template.prompts[i], inputs[i]);
                }
            }
            if (existing == null) {
                nodes.Add(new DialogNode {
                    name = nodeTemplate.name,
                    dialog = dialog
                });
                continue;
            }
            if (!existing.dialog.EndsWith("\n") && existing.dialog != "") {
                existing.dialog += "\n";
            }
            existing.dialog += dialog;
        }
        return nodes;
    }
}
