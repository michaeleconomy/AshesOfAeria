﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class ListAndArrayExtensions {

    public static string Join<T>(this IEnumerable<T> list, string seperator = ", ") {
        var sb = new StringBuilder();
        var putSeperator = false;
        foreach (var t in list) {
            if (putSeperator) {
                sb.Append(seperator);
            }
            else {
                putSeperator = true;
            }
            sb.Append(t);
        }
        return sb.ToString();
    }


    public static string ToSentence<T>(this List<T> list) {
        if (list.Count == 0) {
            return "";
        }
        if (list.Count == 1) {
            return list[0].ToString();
        }

        var sb = new StringBuilder();
        sb.Append(list.GetRange(0, list.Count - 1).Join());
        if (list.Count > 2) {
            sb.Append(",");
        }
        sb.Append(" and ");
        sb.Append(list.Last().ToString());
        return sb.ToString();
    }


    public delegate bool Filter<T>(T item);

    public static T First<T>(this IEnumerable<T> enumerable, Filter<T> filter) {
        foreach (var t in enumerable) {
            if (filter(t)) {
                return t;
            }
        }
        return default(T);
    }

    public static T First<T>(this IEnumerable<T> enumerable) {
        return First(enumerable, (t) => true);
    }


    public static T Last<T>(this IEnumerable<T> enumerable, Filter<T> filter) {
        foreach (var t in enumerable.Reverse()) {
            if (filter(t)) {
                return t;
            }
        }
        return default(T);
    }

    public static bool Contains<T>(this IEnumerable<T> enumerable, Filter<T> matching) where T : class {
        return enumerable.First(matching) != null;
    }

    public static T Last<T>(this IEnumerable<T> enumerable) {
        return Last(enumerable, (t) => true);
    }

    public static bool Empty<T>(this ICollection<T> collection) {
        return collection.Count == 0;
    }

    public static bool Empty<T>(this Stack<T> collection) {
        return collection.Count == 0;
    }

    public static bool Empty<T>(this T[] array) {
        return array.Length == 0;
    }


    public static T[] GetRange<T>(this T[] array, int start, int count = -1) {
        if (count == -1) {
            count = array.Length - start;
        }
        var newArray = new T[count];
        for (var i = 0; i < count; i++) {
            newArray[i] = array[start + i];
        }
        return newArray;
    }


    public static List<T> GetRange<T>(this List<T> array, int start) {
        var count = array.Count - start;
        return array.GetRange(start, count);
    }
}
