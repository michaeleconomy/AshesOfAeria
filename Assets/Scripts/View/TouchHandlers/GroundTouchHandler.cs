﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class GroundTouchHandler : MonoBehaviour, IPointerClickHandler {

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        //Debug.Log("Ground touch");
        if (eventData.dragging || Input.touchCount > 1) {
            return;
        }
        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (World.inEditMode) {
            MapEditor.instance.selectedTool.UseTool(worldClickLocation, null);
            return;
        }
        StartCoroutine(ActionController.instance.DoAction(
            Move(worldClickLocation)));
    }

    IEnumerator Move(Vector3 position) {
        var target = new GameObject("moveTarget");
        target.transform.position = position.RoundPosition();
        yield return StartCoroutine(CurrentPlayerMover.Move(target, 0f));
        Destroy(target);
    }
}
