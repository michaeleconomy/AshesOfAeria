﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiTouchZoomer : MonoBehaviour {

    private bool multiTouching;

    private float lastTouchDistance;

	// Use this for initialization
	void Start () {
        if(!Input.touchSupported) {
            enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("multi touch zoomer update called");
        if (Input.touchCount == 2) {
            var touch1 = Input.touches[0].position;
            var touch2 = Input.touches[1].position;
            var touchDistance = Vector2.Distance(touch1, touch2);
            //Debug.Log("multi touch zoomer - distance: " + touchDistance);
            if (multiTouching) {
                var zoomAmount = lastTouchDistance / touchDistance;
                lastTouchDistance = touchDistance;
                CameraController.ZoomRelative(zoomAmount);
                return;
            }
            multiTouching = true;
            lastTouchDistance = touchDistance;
            return;
        }
        multiTouching = false;
	}
}
