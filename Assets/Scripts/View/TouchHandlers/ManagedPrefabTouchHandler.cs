﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ManagedPrefabTouchHandler : MonoBehaviour,
        IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {
    private ManagedPrefab managedPrefab;

    private void Awake() {
        managedPrefab = GetComponent<ManagedPrefab>();
    }

    public void OnPointerClick(PointerEventData eventData) {
        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (World.inEditMode) {
            if (eventData.dragging) {
                return;
            }
            MapEditor.instance.selectedTool.UseTool(worldClickLocation, managedPrefab);
            return;
        }

        if (managedPrefab is PlayerCharacter) {
            return;
        }
        StartCoroutine(ActionController.instance.DoAction(MoveAndTakeAction()));
    }

    private IEnumerator MoveAndTakeAction() {
        var currentPlayer = TurnManager.instance.CurrentPlayer();
        if (currentPlayer == null) {
            yield break;
        }

        managedPrefab.EnableOutline(Color.green);

        if (managedPrefab is NonPlayerCharacter) {
            var character = (NonPlayerCharacter)managedPrefab;

            if (character.dead) {
                yield return StartCoroutine(CurrentPlayerMover.MoveAndOpenInventory(gameObject));
            }
            else if (character.friendly) {
                yield return StartCoroutine(MoveAndConverse(currentPlayer));
            }
            else {
                yield return StartCoroutine(MoveAndAttack(currentPlayer));
            }
        }
        else if (managedPrefab is Item) {
            yield return StartCoroutine(CurrentPlayerMover.MoveAndOpenInventory(gameObject));
        }
        else {
            if (managedPrefab.hasDialog) {
                yield return StartCoroutine(MoveAndConverse(currentPlayer));
            }
        }
        if (managedPrefab != null) { //this is overloaded to check for deleted.
            managedPrefab.HideOutline();
        }
    }


    private IEnumerator MoveAndAttack(PlayerCharacter currentPlayer) {
        if (!currentPlayer.actions.HasStandardAction()) {
            LogManager.Log(currentPlayer.name + " cannot attack, do not have standard action left");
            yield break;
        }
        var attack = currentPlayer.DefaultAttack();
        var range = attack.Range();
        yield return StartCoroutine(CurrentPlayerMover.Move(gameObject, range, false));

        if (currentPlayer.movement.IsInRange(gameObject, range)) {
            yield return StartCoroutine(attack.Do((GameEntity)managedPrefab));
        }
    }

    private IEnumerator MoveAndConverse(PlayerCharacter currentPlayer) {
        yield return StartCoroutine(CurrentPlayerMover.Move(gameObject, 1f));
        if (currentPlayer.movement.IsInRange(gameObject, 1f)) {
            DialogRunner.instance.StartDialog(managedPrefab);
        }
        else {
            Debug.Log("couldn't get in range");
        }
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        if (!World.inEditMode) {
            return;
        }

        if (Input.touchSupported) {
            if (Input.touchCount > 1) {
                return;
            }
        }
        else {
            if (eventData.button != PointerEventData.InputButton.Left) {
                return;
            }
        }
        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MapEditor.instance.selectedTool.StartToolDrag(worldClickLocation, managedPrefab);
    }

    void IDragHandler.OnDrag(PointerEventData eventData) {
        if (!World.inEditMode) {
            return;
        }

        if (Input.touchSupported) {
            if (Input.touchCount > 1) {
                return;
            }
        }
        else {
            if (eventData.button != PointerEventData.InputButton.Left) {
                return;
            }
        }
        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MapEditor.instance.selectedTool.ToolDrag(worldClickLocation, managedPrefab);
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        if (!World.inEditMode) {
            return;
        }
        if (Input.touchSupported) {
            if (Input.touchCount > 1) {
                return;
            }
        }
        else {
            if (eventData.button != PointerEventData.InputButton.Left) {
                return;
            }
        }
        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MapEditor.instance.selectedTool.EndToolDrag(worldClickLocation, managedPrefab);
    }
}