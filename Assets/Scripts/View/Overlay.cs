﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class Overlay : MonoBehaviour {
    private static Overlay instance;

    public Text text;
    private readonly Stack<string> layers = new Stack<string>();

    protected static Overlay Get() {
        if (instance != null) {
            return instance;
        }
        instance = Resources.FindObjectsOfTypeAll<Overlay>().First();
        if (instance == null) {
            throw new Exception("Couldn't find the Overlay");
        }
        return instance;
    }

    protected void _Show(string message) {
        layers.Push(message);
        //Debug.Log("Overlay.show " + layers.Count);
        text.text = message;
        gameObject.SetActive(true);
    }

    protected void _Hide() {
        if (layers.Empty()) {
            Debug.LogWarning("already hidden!");
            return;
        }
        layers.Pop();
        //Debug.Log("Overlay.hide " + layers.Count);
        if (layers.Empty()){
            gameObject.SetActive(false);
            return;
        }
        text.text = layers.Peek();
    }

    public static void Show(string message = "Please wait") {
        Get()._Show(message);
    }

    public static void Hide() {
        Get()._Hide();
    }
}
