﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ActionOverlay : MonoBehaviour {
    public static ActionOverlay instance;
    public Tilemap actionGrid;
    public TileBase walkTile;
    public TileBase attackTile;
    public TileBase runTile;
    public bool visible;

    private void Awake() {
        instance = this;
    }

    //TODO take an attack as a parameter, and use the range for that attack
    public void Show(PlayerCharacter player) {
        Clear();
        var reachableSpaces = player.movement.GetReachableSpaces(1f);
        foreach (var reachableSpace in reachableSpaces) {
            TileBase tile;
            if (reachableSpace.isAttack) {
                tile = attackTile;
            }
            else if (reachableSpace.run) {
                tile = runTile;
            }
            else {
                tile = walkTile;
            }
            actionGrid.SetTile(reachableSpace.position.In3D(), tile);
        }
        visible = true;
    }

    public void ShowEditMode() {
        Clear();
        var bounds = EditMapManager.instance.GetOverallBounds();
        var layerMask =
            LayerMask.GetMask("Default", "Water", "enemies", "players");
        for (var x = bounds.xMin; x < bounds.xMax; x++) {
            for (var y = bounds.yMin; y < bounds.yMax; y++) {
                var pos = new Vector2Int(x, y);
                var col =
                    Physics2D.OverlapBox(pos.RoundPosition(),
                                         Vector2.one * 0.9f,
                                         0f,
                                         layerMask);
                if (col == null) {
                    actionGrid.SetTile(pos.In3D(), walkTile);
                }
            }
        }
        visible = true;
    }

    public void Clear() {
        actionGrid.ClearAllTiles();
        visible = false;
    }
}
