﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIInventoryItemDetails : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    public static UIInventoryItemDetails instance;

    public Text nameText;


    public GameObject itemDetails;
    public Text valueText;
    public Text weightText;

    public GameObject weaponDetails;
    public Text damageText;
    public Text weaponType;
    public Text weaponProperties;
    public GameObject dropButton;
    public GameObject openButton;

    private UIInventoryGameEntity uiGameEntity;
    public bool mouseLeft = true;
    private RectTransform rectTransform;

    private void Awake() {
        instance = this;
        rectTransform = (RectTransform)transform;
    }

    public void Show(UIInventoryGameEntity uii, bool autohide = false) {
        mouseLeft = false;
        uiGameEntity = uii;

        InitializeForGameEntity();
        Move();
        gameObject.SetActive(true);
        if (autohide) {
            StartCoroutine(HideCheck());
        }
        else {
            StopAllCoroutines();
        }
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void Toggle(UIInventoryGameEntity uii) {
        if (uii != uiGameEntity) {
            Show(uii);
            return;
        }
        if (gameObject.activeSelf) {
            Hide();
            return;
        }
        Show(uii);
    }

    private void Move() {
        var uiItemTransform = ((RectTransform)uiGameEntity.transform);
        float x = uiItemTransform.position.x;
        float y = uiItemTransform.position.y;
        var scaleFactor = GetComponentInParent<Canvas>().scaleFactor;

        if (uiItemTransform.position.x > Camera.main.pixelWidth / 2) {
            x += (uiItemTransform.rect.xMin - rectTransform.rect.width / 2) * scaleFactor;
        }
        else {
            x += (uiItemTransform.rect.xMax + rectTransform.rect.width / 2) * scaleFactor;
        }

        transform.position = new Vector2(x, y);
    }


    public void Drop() {
        if (uiGameEntity is UIInventoryItem) {
            var uiItem = (UIInventoryItem)uiGameEntity;
            uiItem.Drop();
            Hide();
        }
        else {
            throw new Exception("uiGameEntity is not an item and not droppable");
        }
    }

    public void Open() {
        UIInventoryMenu.instance.OpenContainer(uiGameEntity.gameEntity);
    }

    private IEnumerator HideCheck() {
        while (!mouseLeft) {
            yield return new WaitForSeconds(1f);
        }
        Hide();
    }

    private void InitializeForGameEntity() {
        var gameEntity = uiGameEntity.gameEntity;
        nameText.text = gameEntity.DisplayName();
        if (gameEntity is Item) {
            InitializeForItem((Item)gameEntity);
        }
        else {
            itemDetails.SetActive(false);
            dropButton.SetActive(false);
        }
        openButton.SetActive(gameEntity.isContainer);
    }

    private void InitializeForItem(Item item) {
        itemDetails.SetActive(true);
        //TODO - show sp/pp, etc
        valueText.text = item.value.ToString() + "gp";

        //TODO show lbs
        weightText.text = item.Weight().ToString() + "ounces";

        if (item is Weapon) {
            InitializeForWeapon((Weapon)item);
            weaponDetails.SetActive(true);
        }
        else {
            weaponDetails.SetActive(false);
        }
        var slot = uiGameEntity.GetComponentInParent<UIInventorySlot>();
        dropButton.SetActive(slot != null && !slot.ground && !item.questItem);
    }

    private void InitializeForWeapon(Weapon weapon) {
        var attackType =
            weapon.Ranged() ? AttackType.Ranged : AttackType.Melee;
        var attack = new WeaponAttack() {
            weapon = weapon,
            attacker = UIInventoryMenu.instance.character,
            attackType = attackType
        };
        var minDamages = attack.MinDamage();
        var maxDamages = attack.MaxDamage();
        if (minDamages.Count != maxDamages.Count) {
            throw new Exception("inequal types between min and max damage on weapon: " + weapon.name);
        }
        damageText.text = "";
        for (var i = 0; i < maxDamages.Count; i++) {
            var min = minDamages[i];
            var max = maxDamages[i];
            damageText.text += min.amount + " - " + max.amount + " " + max.type + "  ";
        }

        //TODO show if they are proficient here
        //TODO also show a cleaner string (not camel cased)
        weaponType.text = weapon.type.ToString();
        weaponProperties.text = "";
        if (weapon.range > 0) {
            weaponProperties.text += "range(" + weapon.range + ", " + weapon.longRange + ") ";
            if (weapon.thrown) {
                weaponProperties.text += "thrown ";
            }
            else {
                weaponProperties.text += "ammunition ";
            }
        }
        if (weapon.finesse) {
            weaponProperties.text += "finesse ";
        }
        if (weapon.twoHanded) {
            weaponProperties.text += "two-handed ";
        }
        if (weapon.heavy) {
            weaponProperties.text += "heavy ";
        }
        if (weapon.light) {
            weaponProperties.text += "light ";
        }
        if (weapon.loading) {
            weaponProperties.text += "loading ";
        }
        if (weapon.reach) {
            weaponProperties.text += "reach ";
        }
        if (weapon.versatile) {
            weaponProperties.text += "versatile ";
            //TODO show the damage before/after?
        }
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) {
        mouseLeft = false;
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData) {
        mouseLeft = true;
    }
}
