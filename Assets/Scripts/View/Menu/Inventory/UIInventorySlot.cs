﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventorySlot : MonoBehaviour {
    public bool isEquipmentSlot;
    public Equipment.Slot equipmentSlot;
    public bool ground;
    public bool container;
    public bool inventory;
}
