﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIInventoryGameEntity: MonoBehaviour,
        IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
    public GameEntity gameEntity;

    public void InitForItem(GameEntity i) {
        gameEntity = i;
        var image = GetComponent<Image>();
        if (gameEntity is Character) {
            image.sprite = SpriteMunger.Munge(gameEntity);
        }
        else {
            var itemSpriteRenderer = gameEntity.GetComponent<SpriteRenderer>();
            image.sprite = itemSpriteRenderer.sprite;
        }
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) {
        if (eventData.dragging || Input.touchSupported) {
            return;
        }
        UIInventoryItemDetails.instance.Show(this, true);
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData) {
        if (Input.touchSupported) {
            return;
        }
        UIInventoryItemDetails.instance.mouseLeft = true;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        UIInventoryItemDetails.instance.Toggle(this);
    }
}
