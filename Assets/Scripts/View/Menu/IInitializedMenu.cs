﻿using System;
using UnityEngine;

public interface IInitializedMenu {
    void Initialize();
}
