﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UISaveMenu : MonoBehaviour {
    public SaveManager saveManager;

    public void Save() {
        saveManager.NewSave();
        MenuController.instance.CloseAllMenus();
    }

    public void Load() {
        SaveManager.fileToLoad = saveManager.LastSaveFileName();
        SceneManager.LoadScene("Scenes/MainScene");
    }
}
