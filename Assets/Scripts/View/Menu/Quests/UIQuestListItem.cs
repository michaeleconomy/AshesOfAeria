﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestListItem : MonoBehaviour {
    public Text titleText;
    public Text descriptionText;
    public Transform objectivesList;
    public Text objectivePrefab;

    public void Initialize(Quest q, bool completed) {
        titleText.text = q.title;
        descriptionText.text = q.description;
        if (completed) {
            return;
        }
        foreach (var objective in q.objectives) {
            if (objective.order == q.CurrentObjectiveOrder()) {
                var uiObjective =
                    Instantiate(objectivePrefab, objectivesList, false);
                uiObjective.text = " - " + objective.description;
            }
        }
    }
}
