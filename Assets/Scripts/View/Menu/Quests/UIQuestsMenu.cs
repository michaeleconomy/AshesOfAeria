﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestsMenu : MonoBehaviour, IInitializedMenu {
    public GameObject activeTitle;
    public GameObject completedTitle;
    public Transform activeQuestsList;
    public Transform completedQuestsList;
    public ScrollRect scroller;

    public UIQuestListItem questListItemPrefab;

    public void Initialize() {
        Populate(false);
        Populate(true);
        ScrollToTop();
    }

    private void ScrollToTop() {
        StartCoroutine(ScrollToTopLater());
    }

    private IEnumerator ScrollToTopLater() {
        //Do this after one frame so the text has time to appear first
        yield return null;
        scroller.verticalNormalizedPosition = 1f;
    }

    private void Populate(bool completed) {
        List<Quest> quests;
        GameObject title;
        Transform container;
        if (completed) {
            quests = QuestManager.instance.completed;
            title = completedTitle;
            container = completedQuestsList;
        }
        else {
            quests = QuestManager.instance.active;
            title = activeTitle;
            container = activeQuestsList;
        }
        container.DeleteChildren();
        foreach (var q in quests) {
            var listItem = Instantiate(questListItemPrefab, container, false);
            listItem.Initialize(q, completed);
        }
        title.SetActive(quests.Count > 0);
    }
}
