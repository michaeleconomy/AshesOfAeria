﻿using System;
using UnityEngine;
public class HelpTextLinkHandler : TextMeshProLinkTouchHandler {
    public override void HandleTouch(string linkId) {
        switch(linkId) {
            case "foo":
                Debug.Log("foo link clicked");
                break;
            default:
                Debug.Log("HelpTextLinkHandler - unhandled linkId: " + linkId);
                break;
        }
    }
}
