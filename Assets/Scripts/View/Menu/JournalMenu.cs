﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class JournalMenu : MonoBehaviour, IInitializedMenu {
    public ScrollRect logScroller;

    public void Initialize() {
        StartCoroutine(ScrollLogsLater());
    }

    private IEnumerator ScrollLogsLater() {
        //Do this after one frame so the text has time to appear first
        yield return null;
        logScroller.verticalNormalizedPosition = 0f;
    }
}
