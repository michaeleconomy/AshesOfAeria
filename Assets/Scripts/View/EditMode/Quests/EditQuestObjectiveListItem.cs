﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditQuestObjectiveListItem : MonoBehaviour {

    public InputField orderField;
    public InputField keyField;
    public InputField descriptionField;

    public struct ValidatedQuestObjective {
        public string error;
        public Quest.Objective objective;
    }

    public ValidatedQuestObjective GetObjective() {
        int order;
        if (!int.TryParse(orderField.text, out order)) {
            return new ValidatedQuestObjective {
                error = "order must be an integer"
            };
        }
        if (keyField.text == "") {
            return new ValidatedQuestObjective {
                error = "key is required"
            };
        }
        if (descriptionField.text == "") {
            return new ValidatedQuestObjective {
                error = "objective description is required"
            };
        }
        return new ValidatedQuestObjective {
            objective = new Quest.Objective {
                order = order,
                key = keyField.text,
                description = descriptionField.text
            }
        };
    }

    public void Initialize(Quest.Objective objective) {
        orderField.text = objective.order.ToString();
        keyField.text = objective.key;
        descriptionField.text = objective.description;
    }

    public void Remove() {
        Destroy(gameObject);
    }
}
