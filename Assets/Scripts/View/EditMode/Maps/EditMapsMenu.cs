﻿using System;
using UnityEngine;

public class EditMapsMenu : MonoBehaviour, IInitializedMenu {
    public Transform mapsList;
    public EditMapsListItem mapsListItemPrefab;

    public void NewMap() {
        UIDialog.PromptForString("Name of new map", (response) => {
            if (response != null) {
                EditMapManager.instance.New(response);
                EditMenuController.instance.CloseAllMenus();
            }
        });
    }

    public void Initialize() {
        mapsList.DeleteChildren();
        foreach (var mapName in EditMapManager.instance.MapList()) {
            var listItem = Instantiate(mapsListItemPrefab, mapsList, false);
            listItem.Initialize(mapName);
        }
    }
}
