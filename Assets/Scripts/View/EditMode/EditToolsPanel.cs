﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditToolsPanel : MonoBehaviour, IMapEditorToolDelegate {
    public GameObject brushButton;
    public GameObject eraseButton;
    public GameObject cursorButton;
    public GameObject dropperButton;
    public GameObject fillButton;
    public GameObject sprayPaintButton;

    private void Start() {
        MapEditor.instance.toolDelegate = this;
        ToolSelected(MapEditor.instance.selectedTool);
    }

    public void ToolSelected(MapTool tool) {
        UpdateButton(brushButton, tool is BrushTool);
        UpdateButton(eraseButton, tool is EraseTool);
        UpdateButton(cursorButton, tool is CursorTool);
        UpdateButton(dropperButton, tool is DropperTool);
        UpdateButton(fillButton, tool is FillTool);
        UpdateButton(sprayPaintButton, tool is SprayPaintTool);
    }

    private void UpdateButton(GameObject button, bool highlighted) {
        var image = button.GetComponent<Image>();
        if (highlighted) {
            image.color = Color.green;
        }
        else {
            image.color = Color.white;
        }
    }
}
