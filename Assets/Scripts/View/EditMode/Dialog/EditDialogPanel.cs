﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EditDialogPanel : MonoBehaviour {
    public static EditDialogPanel instance;

    public GameObject panel;
    public Text errorText;
    public GameObject errorRow;
    public Text titleText;
    public Transform nodeList;
    public EditDialogNode editDialogNodePrefab;
    public GameObject deleteButton;

    public string speaker;
    private string map;

    private void Awake() {
        instance = this;
    }

    public void Edit(ManagedPrefab managedPrefab) {
        Edit(managedPrefab.name, EditMapManager.instance.currentMap);
    }

    public void Edit(string _speaker, string map) {
        this.map = map;
        speaker = _speaker;
        var dialogNodes = DialogData.instance.GetDialog(map, speaker);
        deleteButton.SetActive(dialogNodes != null);
        titleText.text = "Editing Dialog\n" + map + " / " + speaker;

        if (dialogNodes == null) {
            dialogNodes = new List<DialogNode>{
                new DialogNode {
                    name = DialogRunner.startNodeName,
                    dialog = ""
                }
            };
        }

        UpdateNodes(dialogNodes);
        errorRow.SetActive(false);
        panel.SetActive(true);
    }

    private void UpdateNodes(List<DialogNode> dialogNodes) {
        nodeList.DeleteChildren();
        foreach (var node in dialogNodes) {
            var nodeItem = Instantiate(editDialogNodePrefab, nodeList, false);
            nodeItem.Initialize(node);
        }
    }

    public void AddNode() {
        var dialogNode = Instantiate(editDialogNodePrefab, nodeList, false);
        dialogNode.Initialize(new DialogNode());
    }

    private List<DialogNode> GetNodes() {
        var uiNodes = nodeList.GetComponentsInChildren<EditDialogNode>();
        return uiNodes.Select((uiNode) => uiNode.GetNode()).ToList();
    }

    public void Done() {
        var nodes = GetNodes();

        var errors = DialogValidator.Validate(nodes);
        if (errors.Count > 0) {
            errorText.text = errors.Join("\n");
            errorRow.SetActive(true);
            UIDialog.Confirm("Dialog has errors, save anyway?", () => {
                DialogData.instance.Add(map, speaker, nodes);
                panel.SetActive(false);
            });
            return;
        }
        DialogData.instance.Add(map, speaker, nodes);
        panel.SetActive(false);
    }

    public void Spelling() {
        var errors = DialogValidator.Validate(GetNodes(), true);
        if (errors.Count > 0) {
            errorText.text = errors.Join("\n");
        }
        else {
            errorText.text = "No errors!";
        }
        errorRow.SetActive(true);
    }

    public void PickTemplate() {
        var buttons = DialogTemplates.TemplateNames();
        buttons.Add("cancel");
        UIDialog.Display("Choose a template:",
                         buttons.ToArray(),
                         false, null, (templateId, input) => {
            if (templateId != buttons.Count - 1) {
                StartCoroutine(ApplyTemplate(templateId));
            }
        });
    }

    private IEnumerator ApplyTemplate(int templateId) {
        var prompts = DialogTemplates.PromptsFor(templateId);
        var inputs = new List<string>();
        if (prompts != null) {
            foreach (var prompt in prompts) {
                var done = false;
                UIDialog.PromptForString(prompt, (input) => {
                    inputs.Add(input);
                    done = true;
                });
                while (!done) {
                    yield return null;
                }
            }
        }
        var newNodes =
            DialogTemplates.ApplyTemplate(templateId, GetNodes(), speaker, inputs);
        UpdateNodes(newNodes);
    }

    public void Delete() {
        UIDialog.Confirm("Are you sure?", () => {
            DialogData.instance.Remove(map, speaker);
            panel.SetActive(false);
        });
    }

    public void Cancel() {
        UIDialog.Confirm("Changes will be lost, ok?", () => {
            panel.SetActive(false);
        });
    }
}
