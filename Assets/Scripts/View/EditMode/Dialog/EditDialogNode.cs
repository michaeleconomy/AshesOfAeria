﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditDialogNode : MonoBehaviour {
    public InputField nameText;
    public Text dialogText;

    public void Initialize(DialogNode node) {
        nameText.text = node.name;
        dialogText.text = node.dialog;
    }

    public void Delete() {
        Destroy(gameObject);
    }

    public void EditDialog() {
        var editDialogPanel = GetComponentInParent<EditDialogPanel>();
        var editDialogTextPanel =
            editDialogPanel.GetComponentInChildren<EditDialogTextPanel>(true);
        editDialogTextPanel.Show(this);
    }

    public DialogNode GetNode() {
        return new DialogNode {
            name = nameText.text,
            dialog = dialogText.text
        };
    }
}
