﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditDialogTextPanel : MonoBehaviour {
    public Text nameText;
    public InputField dialogText;
    private EditDialogNode node;

    public void Show(EditDialogNode node) {
        this.node = node;
        nameText.text = node.nameText.text;
        dialogText.text = node.dialogText.text;
        gameObject.SetActive(true);
    }

    public void Save() {
        node.dialogText.text = dialogText.text;
        gameObject.SetActive(false);
    }

    public void Cancel() {
        if (node.dialogText.text != dialogText.text) {
            UIDialog.Confirm("Changes will be lost, ok?", () => {
                gameObject.SetActive(false);
            });
        }
        else {
            gameObject.SetActive(false);
        }

    }
}
