﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ConditionalTesterPanel : MonoBehaviour {
    public DialogConditionals conditionals;
    public InputField input;
    public Text output;

    public void Test() {
        output.text = conditionals.Evaluate(input.text).ToString();
    }
}
