﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditPrefabListItem : MonoBehaviour {
    public ManagedPrefab prefab;
    public Text text;
    public GameObject dialogButton;

    public void Initialize(ManagedPrefab _prefab, string path) {
        prefab = _prefab;
        text.text = prefab.name + "\n" + path;
        dialogButton.SetActive(prefab.HasDialog());
    }

    public void Edit() {
        EditMenuController.instance.CloseAllMenus();
        CameraController.CenterOn(prefab);
        MapEditor.instance.ShowDetails(prefab);
    }

    public void Dialog() {
        EditDialogPanel.instance.Edit(prefab);
    }

    public void Delete() {
        UIDialog.Confirm("Are you sure you want to delete " +
                         prefab.name + "?", () => {
            prefab.Destroy();
            Destroy(gameObject);
        });
    }
}
