﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text.RegularExpressions;

public class EditEntityDetails : MonoBehaviour, IMapEditorEntityDetailsDelegate {
    public static EditEntityDetails instance;

    public ManagedPrefab managedPrefab;
    public Text nameText;
    public Toggle activeToggle;
    public Toggle trackDeathToggle;
    public GameObject editDialogButton;
    public GameObject appearanceButton;
    public GameObject scaleRow;
    public Slider scaleSlider;

    //inventory stuff
    public GameObject contentsPanel;
    public Transform contentsContainer;
    public GameObject equipmentPanel;
    public Transform equipmentContainer;
    public GameObject newItemsPanel;
    public Transform newItemsContainer;

    public PrefabManager prefabManager;

    public HumanoidBodyAppearancePanel humanoidBodyAppearancePanel;

    private void Awake() {
        instance = this;
        newItemsContainer.DeleteChildren();
        var items = prefabManager.GetAllPrefabsOfType<Item>();
        foreach (var item in items) {
            DetailsItemChooser.Create(item, false, newItemsContainer);
        }
        scaleSlider.onValueChanged.AddListener(Scale);
    }

    private void Start() {
        MapEditor.instance.detailsDelegate = this;
        gameObject.SetActive(false);
    }

    void IMapEditorEntityDetailsDelegate.ShowDetails(ManagedPrefab managedPrefab) {
        this.managedPrefab = managedPrefab;
        var invisible = managedPrefab is InvisibleManagedPrefab;
        nameText.text = managedPrefab.name;
        activeToggle.isOn = managedPrefab.active;
        activeToggle.gameObject.SetActive(!invisible);

        if (managedPrefab is Character) {
            equipmentContainer.DeleteChildren();
            var character = (Character)managedPrefab;
            foreach (var equipment in character.startingEquipment) {
                DetailsItemChooser.Create(equipment, true, equipmentContainer);
            }
            equipmentPanel.SetActive(true);
        }
        else {
            equipmentPanel.SetActive(false);
        }
        InitializeTrackDeathToggle();

        ShowContents();
        editDialogButton.SetActive(managedPrefab.HasDialog());
        var body = managedPrefab.GetComponent<HumanoidBody>();
        appearanceButton.SetActive(body != null);

        transform.position = Camera.main.pixelRect.size / 2;
        scaleRow.SetActive(managedPrefab.scalable);
        scaleSlider.value = managedPrefab.transform.localScale.x;
        gameObject.SetActive(true);
    }

    private void InitializeTrackDeathToggle() {
        if (managedPrefab is NonPlayerCharacter) {
            trackDeathToggle.isOn = ((NonPlayerCharacter)managedPrefab).trackDeath;
            trackDeathToggle.gameObject.SetActive(true);
            return;
        }
        trackDeathToggle.gameObject.SetActive(false);
    }

    private void ShowContents() {
        if (managedPrefab is GameEntity) {
            var gameEntity = (GameEntity)managedPrefab;
            if (gameEntity.isContainer) {
                contentsContainer.DeleteChildren();
                foreach (var item in gameEntity.startingContents) {
                    DetailsItemChooser.Create(item, true, contentsContainer);
                }
                contentsPanel.SetActive(true);
                newItemsPanel.SetActive(true);
                return;
            }
        }

        contentsPanel.SetActive(false);
        newItemsPanel.SetActive(false);
    }

    public void AddItem(DetailsItemChooser uiItem) {
        if (!(managedPrefab is GameEntity)) {
            throw new Exception(managedPrefab.name +
                                " is not a GameEntity and can't hold objects!");
        }
        var item = uiItem.item;
        var gameEntity = (GameEntity)managedPrefab;
        if (item is Equipment && gameEntity is Character) {
            var character = (Character)gameEntity;
            if (!character.startingEquipment.Contains((Equipment)item)) {
                character.startingEquipment.Add((Equipment)item);
                DetailsItemChooser.Create(item, true, equipmentContainer);
            }
            else {
                gameEntity.startingContents.Add(item);
                DetailsItemChooser.Create(item, true, contentsContainer);
            }
        }
        else {
            gameEntity.startingContents.Add(item);
            DetailsItemChooser.Create(item, true, contentsContainer);
        }
        gameEntity.RefreshSubItems();
    }

    public void RemoveItem(DetailsItemChooser uiItem) {
        if (!(managedPrefab is GameEntity)) {
            throw new Exception(managedPrefab.name +
                                " is not a GAmeEntity and can't hold objects!");
        }
        var item = uiItem.item;
        var gameEntity = (GameEntity)managedPrefab;
        if (gameEntity is Character && item is Equipment) {
            var character = (Character)gameEntity;
            character.startingEquipment.Remove((Equipment)item);
        }
        else {
            gameEntity.startingContents.Remove(item);
        }
        Destroy(uiItem.gameObject);
        gameEntity.RefreshSubItems();
    }

    public void ToggleActive() {
        managedPrefab.SetActive(activeToggle.isOn);
    }

    public void ToggleTrackDeath() {
        var npc = (NonPlayerCharacter)managedPrefab;
        npc.trackDeath = trackDeathToggle.isOn;
    }

    public void EditName() {
        UIDialog.PromptForString("Renaming: ", managedPrefab.name, (newName) => {
            if (newName == null) {
                return;
            }
            var oldName = managedPrefab.name;
            if (newName == oldName) {
                return;
            }
            if (!ManagedPrefab.ValidName(newName)) {
                UIDialog.Alert("invalid name, must only contain word characters");
                return;
            }
            if (World.instance.FindChildByName<ManagedPrefab>(newName) != null) {
                UIDialog.Alert("duplicate name!");
                return;
            }
            managedPrefab.name = newName;
            nameText.text = newName;
            var map = EditMapManager.instance.currentMap;
            DialogData.instance.RenameSpeaker(map, oldName, newName);
        });
    }

    public void EditDialog() {
        EditDialogPanel.instance.Edit(managedPrefab);
    }

    public void EditAppearance() {
        var body = managedPrefab.GetComponent<HumanoidBody>();
        if (body == null) {
            Debug.LogWarning("can't open panel: no body found");
            return;
        }
        gameObject.SetActive(false);
        humanoidBodyAppearancePanel.Initialize(body);
    }

    public void Delete() {
        UIDialog.Confirm("Delete " + managedPrefab.name + " are you sure?", () => {
            managedPrefab.Destroy();
            gameObject.SetActive(false);
        });
    }

    public void Scale(float value) {
        managedPrefab.transform.localScale = Vector3.one * value;
    }
}
