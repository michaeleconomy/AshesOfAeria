﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ColorPickerRow : MonoBehaviour {
    public Image colorLabel;

    public GameObject picker;
    public Slider hueSlider;
    public Slider satSlider;
    public Slider valSlider;

    public delegate void ColorChanged(Color color);
    public ColorChanged changeDelegate;

    private float h, s, v;

    private void Awake() {
        hueSlider.onValueChanged.AddListener(SetHue);
        satSlider.onValueChanged.AddListener(SetSat);
        valSlider.onValueChanged.AddListener(SetVal);
    }

    public void Initialize(Color color) {
        Color.RGBToHSV(color, out h, out s, out v);
        UpdateUIColor();
        picker.SetActive(false);
    }

    private void UpdateUIColor() {
        hueSlider.value = h;
        SetSlider(satSlider, s, Color.HSVToRGB(h, 1, v));
        var satBG = GetBackgroundImage(satSlider);
        if (satBG != null && satBG.transform.childCount > 0) {
            var satFG = satBG.transform.GetChild(0).GetComponent<Image>();
            if (satFG != null) {
                satFG.color = Color.HSVToRGB(h, 0, v);
            }
        }
        SetSlider(valSlider, v, Color.HSVToRGB(h, s, 1));
        colorLabel.color = CurrentColor();
    }

    private void SetSlider(Slider slider, float value, Color color) {
        slider.value = value;

        var image = GetBackgroundImage(slider);
        if (image == null) {
            Debug.LogWarning("background image could not be found");
            return;
        }

        image.color = color;
    }

    private Image GetBackgroundImage(Slider slider) {
        var backgroundNode = slider.transform.Find("Background");
        if (backgroundNode == null) {
            Debug.LogWarning("background node could not be found");
            return null;
        }
        return backgroundNode.GetComponent<Image>();
    }

    public void TogglePicker() {
        if (picker.activeSelf) {
            picker.SetActive(false);
            return;
        }
        GetComponentInParent<HumanoidBodyAppearancePanel>().HidePickers();
        picker.SetActive(true);
    }

    public void SetHue(float _hue) {
        h = _hue;
        SetColor();
    }

    public void SetSat(float _sat) {
        s = _sat;
        SetColor();
    }

    public void SetVal(float _val) {
        v = _val;
        SetColor();
    }

    private void SetColor() {
        UpdateUIColor();
        changeDelegate(CurrentColor());
    }

    public void SetColor(Image image) {
        Color.RGBToHSV(image.color, out h, out s, out v);
        SetColor();
    }

    private Color CurrentColor() {
        return Color.HSVToRGB(h, s, v);
    }


    public void Randomize() {
        var color = UnityEngine.Random.ColorHSV();
        Color.RGBToHSV(color, out h, out s, out v);
        SetColor();
    }
}
