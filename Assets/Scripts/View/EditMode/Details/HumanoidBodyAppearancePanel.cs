﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HumanoidBodyAppearancePanel : MonoBehaviour {
    public ColorPickerRow skinColorPickerRow;

    public ColorPickerRow hairColorPickerRow;
    public AppearanceDropdown hairNumberRow;
    public AppearanceDropdown hairBackNumberRow;
    public AppearanceDropdown facialHairNumberRow;

    public ColorPickerRow shirtColorPickerRow;
    public AppearanceDropdown shirtNumberRow;
    public ColorPickerRow pantsColorPickerRow;
    public AppearanceDropdown pantsNumberRow;
    public ColorPickerRow shoeColorPickerRow;

    public ColorPickerRow headAccessoryColorPickerRow;
    public AppearanceDropdown headAccessoryNumberRow;
    public ColorPickerRow torsoAccessoryColorPickerRow;
    public AppearanceDropdown torsoAccessoryNumberRow;

    private HumanoidBody body;
    private SavedHumanoidBody savedHumanoidBody;

    private void Awake() {
        skinColorPickerRow.changeDelegate = (color) => {
            body.skinColor = color;
            body.RefreshAppearance();
        };

        hairNumberRow.changeDelegate = (num) => {
            body.hairNumber = num;
            hairColorPickerRow.gameObject.SetActive(num >= 0);
            body.RefreshAppearance();
        };
        hairColorPickerRow.changeDelegate = (color) => {
            body.hairColor = color;
            body.RefreshAppearance();
        };
        hairBackNumberRow.changeDelegate = (num) => {
            body.hairBackNumber = num;
            body.RefreshAppearance();
        };
        facialHairNumberRow.changeDelegate = (num) => {
            body.facialHairNumber = num;
            body.RefreshAppearance();
        };

        shirtNumberRow.changeDelegate = (num) => {
            body.shirtNumber = num;
            body.RefreshAppearance();
        };
        shirtColorPickerRow.changeDelegate = (color) => {
            body.shirtColor = color;
            body.RefreshAppearance();
        };

        pantsNumberRow.changeDelegate = (num) => {
            body.pantsNumber = num;
            body.RefreshAppearance();
        };
        pantsColorPickerRow.changeDelegate = (color) => {
            body.pantsColor = color;
            body.RefreshAppearance();
        };

        shoeColorPickerRow.changeDelegate = (color) => {
            body.shoeColor = color;
            body.RefreshAppearance();
        };

        headAccessoryNumberRow.changeDelegate = (num) => {
            body.headAccessoryNumber = num;
            headAccessoryColorPickerRow.gameObject.SetActive(num >= 0);
            body.RefreshAppearance();
        };
        headAccessoryColorPickerRow.changeDelegate = (color) => {
            body.headAccessoryColor = color;
            body.RefreshAppearance();
        };

        torsoAccessoryNumberRow.changeDelegate = (num) => {
            body.torsoAccessoryNumber = num;
            torsoAccessoryColorPickerRow.gameObject.SetActive(num >= 0);
            body.RefreshAppearance();
        };
        torsoAccessoryColorPickerRow.changeDelegate = (color) => {
            body.torsoAccessoryColor = color;
            body.RefreshAppearance();
        };
    }

    public void HidePickers() {
        skinColorPickerRow.picker.SetActive(false);
        hairColorPickerRow.picker.SetActive(false);
        shirtColorPickerRow.picker.SetActive(false);
        pantsColorPickerRow.picker.SetActive(false);
        shoeColorPickerRow.picker.SetActive(false);
        headAccessoryColorPickerRow.picker.SetActive(false);
        torsoAccessoryColorPickerRow.picker.SetActive(false);
    }

    public void Initialize(HumanoidBody body) {
        this.body = body;
        savedHumanoidBody = new SavedHumanoidBody(body);
        skinColorPickerRow.Initialize(body.skinColor);
        hairNumberRow.Initialize(true, body.hairNumber, GetOptions(body.hairSprites));
        hairColorPickerRow.Initialize(body.hairColor);
        hairColorPickerRow.gameObject.SetActive(body.hairNumber >= 0);
        hairBackNumberRow.Initialize(true, body.hairBackNumber, GetOptions(body.hairBackSprites));
        facialHairNumberRow.Initialize(true, body.facialHairNumber, GetOptions(body.facialHairSprites));

        shirtNumberRow.Initialize(false, body.shirtNumber, GetOptions(body.shirtSprites));
        shirtColorPickerRow.Initialize(body.shirtColor);

        pantsNumberRow.Initialize(false, body.pantsNumber, GetOptions(body.pantsSprites));
        pantsColorPickerRow.Initialize(body.pantsColor);

        shoeColorPickerRow.Initialize(body.shoeColor);

        headAccessoryNumberRow.Initialize(true, body.headAccessoryNumber, GetOptions(body.headAccessorySprites));
        headAccessoryColorPickerRow.Initialize(body.headAccessoryColor);
        headAccessoryColorPickerRow.gameObject.SetActive(
            body.headAccessoryNumber >= 0 && body.headAccessorySprites.Count > 0);

        torsoAccessoryNumberRow.Initialize(true, body.torsoAccessoryNumber, GetOptions(body.torsoAccessorySprites));
        torsoAccessoryColorPickerRow.Initialize(body.torsoAccessoryColor);
        torsoAccessoryColorPickerRow.gameObject.SetActive(
            body.torsoAccessoryNumber >= 0 && body.torsoAccessorySprites.Count > 0);

        gameObject.SetActive(true);
        CameraController.EditAngle(body);
    }

    private List<string> GetOptions(List<Sprite> sprites) {
        var list = sprites.Select((s) => s.name).ToList();
        if (list.Count > 1) {
            var lettersMatch = 0;
            var done = false;
            for (; lettersMatch < list[0].Length; lettersMatch++) {
                var letter = list[0][lettersMatch];
                for (var i = 1; i < list.Count; i++) {
                    if (letter != list[i][lettersMatch]) {
                        done = true;
                        break;
                    }
                }
                if (done) {
                    break;
                }
            }

            if (lettersMatch > 0) {
                for (var i = 0; i < list.Count; i++) {
                    list[i] = list[i].Substring(lettersMatch);
                }
            }
        }

        return list;
    }

    public void RandomAppearance() {
        skinColorPickerRow.Randomize();
        hairColorPickerRow.Randomize();
        hairNumberRow.Randomize();
        hairBackNumberRow.Randomize();
        facialHairNumberRow.Randomize();
        RandomClothing();
    }

    public void RandomClothing() {
        shirtNumberRow.Randomize();
        shirtColorPickerRow.Randomize();

        pantsNumberRow.Randomize();
        pantsColorPickerRow.Randomize();

        shoeColorPickerRow.Randomize();

        headAccessoryNumberRow.Randomize();
        headAccessoryColorPickerRow.Randomize();

        torsoAccessoryNumberRow.Randomize();
        torsoAccessoryColorPickerRow.Randomize();
    }

    public void Revert() {
        UIDialog.Confirm("Revert current changes, are you sure?", () => {
            savedHumanoidBody.Restore(body);
        });
    }

    public void Close() {
        gameObject.SetActive(false);
    }
}
