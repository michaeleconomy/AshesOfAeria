﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditMenuController : MonoBehaviour {
    public static EditMenuController instance;

    //edit mode
    public GameObject editMenu;
    public GameObject editQuestsMenu;
    public GameObject editMapsMenu;
    public GameObject prefabsMenu;
    public GameObject triggersMenu;
    public GameObject mapSavesMenu;
    public GameObject helpMenu;

    public GameObject objectChooser;
    public GameObject editDialogPanel;
    public GameObject entityDetailsPanel;
    public GameObject humanoidBodyAppearancePanel;

    //edit mode hud
    public GameObject editToolsPanel;
    public GameObject editObjectsPanel;

    private void Awake() {
        instance = this;
    }

    public void ToggleMenu() {
        if (MenusOpen()) {
            CloseAllMenus();
            return;
        }
        editMenu.SetActive(true);
        HideHud();
    }

    public void CloseAllMenus() {
        HideOtherMenus();
        ShowHud();
    }

    private void ShowMenu(GameObject m) {
        HideOtherMenus();
        var initializedMenu = m.GetComponent<IInitializedMenu>();
        if (initializedMenu != null) {
            initializedMenu.Initialize();
        }
        m.SetActive(true);
    }

    private void ShowHud() {
        editToolsPanel.SetActive(true);
        editObjectsPanel.SetActive(true);
    }

    private void HideHud() {
        editToolsPanel.SetActive(false);
        editObjectsPanel.SetActive(false);
    }

    public void ShowEditQuests() {
        ShowMenu(editQuestsMenu);
    }

    public void ShowEditMaps() {
        ShowMenu(editMapsMenu);
    }


    public void ShowMapSavesMenu() {
        ShowMenu(mapSavesMenu);
    }


    public void ShowHelpMenu() {
        ShowMenu(helpMenu);
    }

    public void ShowEditPrefabs() {
        ShowMenu(prefabsMenu);
    }

    public void ShowEditTriggers() {
        ShowMenu(triggersMenu);
    }



    public void MainMenu() {
        UIDialog.Confirm("Go to main menu? all unsaved changes will be lost.",
                         () => {
            EditMapManager.instance.SaveCurrentBaseMap();
            SceneManager.LoadScene("Scenes/MainMenu");
        });
    }


    private bool MenusOpen() {
        return editMenu.activeSelf ||
                   objectChooser.activeSelf ||
                   editQuestsMenu.activeSelf ||
                   editDialogPanel.activeSelf ||
                   entityDetailsPanel.activeSelf ||
                   mapSavesMenu.activeSelf ||
                   editMapsMenu.activeSelf ||
                   prefabsMenu.activeSelf ||
                   humanoidBodyAppearancePanel.activeSelf ||
                   triggersMenu.activeSelf;
    }

    private void HideOtherMenus() {
        editMenu.SetActive(false);
        objectChooser.SetActive(false);
        editQuestsMenu.SetActive(false);
        editDialogPanel.SetActive(false);
        entityDetailsPanel.SetActive(false);
        mapSavesMenu.SetActive(false);
        editMapsMenu.SetActive(false);
        prefabsMenu.SetActive(false);
        humanoidBodyAppearancePanel.SetActive(false);
        triggersMenu.SetActive(false);
    }
}
