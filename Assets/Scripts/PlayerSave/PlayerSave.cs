﻿using System;
using System.Collections.Generic;

[Serializable]
public class PlayerSave {
    public List<PlayerSavedMap> savedMaps;
    public string currentMap;
    public SavedTurnManager savedTurnManager;
    public string primaryPlayerId;
    public SavedQuestManager savedQuestManager;
    public SavedDialogVariables savedVariables;
    public SavedRespawner savedRespawner;
    public List<ReadiedTrigger> triggers;
    public int currentTime;
}
