﻿using System;
public class SavedNonPlayerCharacter : SavedCharacter {
    public bool friendly;
    public bool trackDeath;

    public SavedNonPlayerCharacter() : base() { }

    public SavedNonPlayerCharacter(NonPlayerCharacter c) : base(c) {
        friendly = c.friendly;
        trackDeath = c.trackDeath;
    }


    public override ManagedPrefab Restore(ManagedPrefab prefab) {
        var c = (NonPlayerCharacter)base.Restore(prefab);
        c.friendly = friendly;
        c.trackDeath = trackDeath;
        return c;
    }
}
