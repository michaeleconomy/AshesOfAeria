﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class SavedCharacter : SavedGameEntity {
    public int strength;
    public int dexterity;
    public int constitution;
    public int intelligence;
    public int wisdom;
    public int charisma;
    public SavedHumanoidBody savedHumanoidBody;


    //TODO LEVELS/CLASSES/XP

    public CharacterActions actions;

    public bool dead;
    public List<string> equipmentIds = new List<string>();

    protected SavedCharacter() { }

    protected SavedCharacter(Character c) : base(c) {
        strength = c.strength;
        dexterity = c.dexterity;
        constitution = c.constitution;
        intelligence = c.intelligence;
        wisdom = c.wisdom;
        actions = c.actions;
        dead = c.dead;
        AddCharacterEquipment(c.equipment);
        var body = c.GetComponent<HumanoidBody>();
        if (body != null) {
            savedHumanoidBody = new SavedHumanoidBody(body);
        }
    }

    private void AddCharacterEquipment(CharacterEquipment e) {
        AddEquipment(e.armor);
        AddEquipment(e.belt);
        AddEquipment(e.gloves);
        AddEquipment(e.helm);
        AddEquipment(e.shoes);
        AddEquipment(e.bracelets);
        AddEquipment(e.hands);
        AddEquipment(e.necklaces);
        AddEquipment(e.rings);
    }

    private void AddEquipment(Equipment e) {
        if(e != null) {
            equipmentIds.Add(e.Id());
        }
    }

    private void AddEquipment(List<Equipment> l) {
        foreach (var e in l) {
            AddEquipment(e);
        }
    }


    public override ManagedPrefab Restore(ManagedPrefab prefab) {
        var c = (Character)base.Restore(prefab);

        c.strength = strength;
        c.dexterity = dexterity;
        c.constitution = constitution;
        c.intelligence = intelligence;
        c.wisdom = wisdom;
        c.actions = actions;
        c.actions.character = c;
        c.dead = dead;
        c.OnLoad();
        var body = c.GetComponent<HumanoidBody>();
        if (body!= null && savedHumanoidBody != null) {
            savedHumanoidBody.Restore(body);
        }

        return c;
    }

    public override void LoadReferences(ManagedPrefab savablePrefab,
            Dictionary<string, ManagedPrefab> savablePrefabs) {
        base.LoadReferences(savablePrefab, savablePrefabs);
        var character = (Character)savablePrefab;
        foreach (var equipmentId in equipmentIds) {
            var equipment = (Equipment)savablePrefabs[equipmentId];
            character.Equip(equipment);
        }
    }
}
