﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SavedGameEntity : SavedPrefab {
    public int hp;
    public int hpMax;
    public List<string> contentIds = new List<string>();

    public SavedGameEntity() {
        //nothing
    }

    public SavedGameEntity(GameEntity gameEntity) : base(gameEntity) {
        hp = gameEntity.hp;
        hpMax = gameEntity.hpMax;

        if (prefabId == 0) {
            Debug.LogWarning(gameEntity.name + "'s prefabId hasn't been set");
        }
        foreach(var item in gameEntity.contents) {
            contentIds.Add(item.Id());
        }
    }

    public override ManagedPrefab Restore(ManagedPrefab prefab) {
        var gameEntity = (GameEntity)base.Restore(prefab);
        gameEntity.hp = hp;
        hpMax = gameEntity.hpMax;
        return gameEntity;
    }

    public override void LoadReferences(ManagedPrefab managedPrefab,
                                        Dictionary<string, ManagedPrefab> managedPrefabs) {
        base.LoadReferences(managedPrefab, managedPrefabs);
        var gameEntity = (GameEntity)managedPrefab;
        foreach (var contentId in contentIds) {
            var item = (Item)managedPrefabs[contentId];
            gameEntity.Add(item);
        }
    }
}
