﻿using System;
using System.Collections.Generic;

[Serializable]
public class PlayerSavedMap {
    public string name;
    public List<SavedPrefab> savedPrefabs;
}
