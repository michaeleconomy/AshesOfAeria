﻿using System;
public class SavedPlayerCharacter : SavedCharacter {
    public int deathSavesSucceeded;
    public int deathSavesFailed;
    public int xp;

    public bool stable;

    public SavedPlayerCharacter() : base() { }

    public SavedPlayerCharacter(PlayerCharacter c) : base(c) {
        deathSavesSucceeded = c.deathSavesSucceeded;
        deathSavesFailed = c.deathSavesFailed;
        stable = c.stable;
        xp = c.xp;
    }

    public override ManagedPrefab Restore(ManagedPrefab prefab) {
        var c = (PlayerCharacter)base.Restore(prefab);

        c.deathSavesSucceeded = deathSavesSucceeded;
        c.deathSavesFailed = deathSavesFailed;
        c.stable = stable;
        c.xp = xp;

        return c;
    }
}
