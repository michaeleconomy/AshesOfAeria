﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SavedQuestManager {
    public List<string> completedQuestIds;
    public List<string> activeQuestIds;
    public List<string> completedObjectives;

    public SavedQuestManager() {

    }

    private SavedQuestManager(QuestManager questManager) {
        completedQuestIds = questManager.completed.Select((arg) => arg.id).ToList();
        activeQuestIds = questManager.active.Select((arg) => arg.id).ToList();
        completedObjectives = questManager.objectivesCompleted.ToList();
    }


    public void Restore() {
        var questManager = QuestManager.instance;
        questManager.active.Clear();
        questManager.completed.Clear();
        foreach (var id in completedQuestIds) {
            var q = questManager.questsById[id];
            questManager.completed.Add(q);
        }
        foreach (var id in activeQuestIds) {
            var q = questManager.questsById[id];
            questManager.active.Add(q);
        }
        questManager.objectivesCompleted = new HashSet<string>(completedObjectives);
    }

    public static SavedQuestManager Save() {
        return new SavedQuestManager(QuestManager.instance);
    }
}
