﻿using System;
using UnityEngine;

[Serializable]
public class SavedColor {
    public float r;
    public float g;
    public float b;
    public float a;

    public SavedColor(){ }

    public SavedColor(Color c) {
        r = c.r;
        g = c.g;
        b = c.b;
        a = c.a;
    }

    public Color Color() {
        return new Color(r, g, b, a);
    }
}
