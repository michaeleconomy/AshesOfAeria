﻿using System;
using UnityEngine;

[Serializable]
public class SavedHumanoidBody {

    public SavedColor skinColor;

    public SavedColor hairColor;
    public int hairNumber;
    public int hairBackNumber;
    public int facialHairNumber;

    public SavedColor shirtColor;
    public int shirtNumber;

    public SavedColor pantsColor;
    public int pantsNumber;

    public SavedColor shoeColor;

    public SavedColor headAccessoryColor;
    public int headAccessoryNumber;

    public SavedColor torsoAccessoryColor;
    public int torsoAccessoryNumber;


    public SavedHumanoidBody() { }

    public SavedHumanoidBody(HumanoidBody body) {
        skinColor = new SavedColor(body.skinColor);
        hairColor = new SavedColor(body.hairColor);
        hairNumber = body.hairNumber;
        hairBackNumber = body.hairBackNumber;
        facialHairNumber = body.facialHairNumber;
        shirtColor = new SavedColor(body.shirtColor);
        shirtNumber = body.shirtNumber;
        pantsColor = new SavedColor(body.pantsColor);
        pantsNumber = body.pantsNumber;
        shoeColor = new SavedColor(body.shoeColor);
        headAccessoryColor = new SavedColor(body.headAccessoryColor);
        headAccessoryNumber = body.headAccessoryNumber;
        torsoAccessoryColor = new SavedColor(body.torsoAccessoryColor);
        torsoAccessoryNumber = body.torsoAccessoryNumber;
    }

    public void Restore(HumanoidBody body) {
        body.skinColor = skinColor.Color();
        body.hairColor = hairColor.Color();
        body.hairNumber = hairNumber;
        body.hairBackNumber = hairBackNumber;
        body.facialHairNumber = facialHairNumber;
        body.shirtColor = shirtColor.Color();
        body.shirtNumber = shirtNumber;
        body.pantsColor = pantsColor.Color();
        body.pantsNumber = pantsNumber;
        body.shoeColor = shoeColor.Color();
        if (headAccessoryColor != null) {
            body.headAccessoryColor = headAccessoryColor.Color();
        }
        body.headAccessoryNumber = headAccessoryNumber;
        if (torsoAccessoryColor != null) {
            body.torsoAccessoryColor = torsoAccessoryColor.Color();
        }
        body.torsoAccessoryNumber = torsoAccessoryNumber;

        body.RefreshAppearance();
    }
}
