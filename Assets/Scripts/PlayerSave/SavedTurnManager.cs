﻿using System;
using System.Collections.Generic;

[Serializable]
public class SavedTurnManager {

    public string activeCharacterId;
    public int currentInitative;
    public List<string> characterIds;
    public List<int> initiativeRolls;


    public SavedTurnManager() { }

    public SavedTurnManager(TurnManager turnManager) {
        characterIds = new List<string>();
        initiativeRolls = new List<int>();
        activeCharacterId = turnManager.activeCharacter.Id();
        currentInitative = turnManager.currentInitative;
        foreach (var initative in turnManager.initatives.Keys) {
            characterIds.Add(initative.character.Id());
            initiativeRolls.Add(initative.roll);
        }
    }

    public void Load(Dictionary<string, ManagedPrefab> savablePrefabs) {
        var tm = TurnManager.instance;

        for (int i = 0; i < characterIds.Count; i++) {
            var characterId = characterIds[i];
            var character = (Character)savablePrefabs[characterId];
            var initativeRoll = initiativeRolls[i];
            var initative = character.RollInitative(initativeRoll);
            tm.AddInitative(initative);
        }
        tm.currentInitative = currentInitative;
        tm.activeCharacter = (Character)savablePrefabs[activeCharacterId];
    }
}
