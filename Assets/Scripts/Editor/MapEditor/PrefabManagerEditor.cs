﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PrefabManager))]
public class PrefabManagerEditor : Editor {

    public override void OnInspectorGUI() {
        var prefabManager = (PrefabManager)target;
        if (GUILayout.Button("Reindex")) {
            prefabManager.Reindex();
        }
        base.OnInspectorGUI();
    }
}
