﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TileManager))]
public class TileManagerEditor : Editor {

    public override void OnInspectorGUI() {
        var tileManager = (TileManager)target;
        if (GUILayout.Button("Reload")) {
            tileManager.Reload();
        }
        base.OnInspectorGUI();
    }
}
