﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(TurnManager))]
public class TurnManagerEditor : Editor {
    bool showObjectives;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        var turnManager = (TurnManager)target;
        if (Application.isPlaying) {
            foreach (var initiative in turnManager.initatives.Keys) {
                EditorGUILayout.LabelField(initiative.Score().ToString(),
                                           initiative.character.name);
            }
        }
    }
}