﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

[CustomEditor(typeof(TriggerManager))]
public class TriggerManagerEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        var triggerManager = (TriggerManager)target;
        foreach (var trigger in triggerManager.readiedTriggers) {
            GUILayout.Label(trigger.name + " " + trigger.mapName);
        }
    }
}