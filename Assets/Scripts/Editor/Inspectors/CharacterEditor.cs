﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CharacterEditor : GameEntityEditor {

    SerializedProperty equipmentProperty;
    SerializedProperty startingEquipmentProperty;
    SerializedProperty characterActionsProperty;
    SerializedProperty moveSpeedProperty;

    protected override void OnEnable() {
        base.OnEnable();
        equipmentProperty = serializedObject.FindProperty("equipment");
        startingEquipmentProperty = serializedObject.FindProperty("startingEquipment");
        characterActionsProperty = serializedObject.FindProperty("actions");
        moveSpeedProperty = serializedObject.FindProperty("moveSpeed");
    }

    public override void OnInspectorGUI() {
        var character = (Character)target;
        character.hp = EditorGUILayout.IntField("HP", character.hp);
        character.hpMax = EditorGUILayout.IntField("HP Max", character.hpMax);
        EditorGUILayout.LabelField("AC", character.ArmorClass().ToString());
        character.gender = (Gender)EditorGUILayout.EnumPopup("Gender", character.gender);
        //character.hair = 
        character.strength = EditorGUILayout.IntField("Strength", character.strength);
        character.dexterity = EditorGUILayout.IntField("Dexterity", character.dexterity);
        character.constitution = EditorGUILayout.IntField("Constitution", character.constitution);
        character.intelligence = EditorGUILayout.IntField("intelligence", character.intelligence);
        character.wisdom = EditorGUILayout.IntField("Wisdom", character.wisdom);
        character.charisma = EditorGUILayout.IntField("Charisma", character.charisma);
        moveSpeedProperty.intValue = EditorGUILayout.IntField("move speed", moveSpeedProperty.intValue);

        character.genericName = EditorGUILayout.TextField("Generic Name", character.genericName);
        EditorGUILayout.PropertyField(startingContentsProperty, true);
        EditorGUILayout.PropertyField(startingEquipmentProperty, true);
        if (Application.isPlaying) {
            EditorGUILayout.PropertyField(contentsProperty, true);
            EditorGUILayout.PropertyField(equipmentProperty, true);
            EditorGUILayout.PropertyField(characterActionsProperty, true);
        }

        serializedObject.ApplyModifiedProperties();
        if (GUI.changed) {
            EditorUtility.SetDirty(character);
        }
    }
}