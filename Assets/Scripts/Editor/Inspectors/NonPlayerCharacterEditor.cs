﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

[CustomEditor(typeof(NonPlayerCharacter))]
[CanEditMultipleObjects]
public class NonPlayerCharacterEditor : CharacterEditor {

    SerializedProperty customAttacksProperty;

    protected override void OnEnable() {
        base.OnEnable();
        customAttacksProperty = serializedObject.FindProperty("customAttacks");
    }

    public override void OnInspectorGUI() {
        var character = (NonPlayerCharacter)target;
        character.friendly = EditorGUILayout.Toggle("Friendly", character.friendly);
        character.xpValue = EditorGUILayout.IntField("XP Value", character.xpValue);
        if (EditorGUILayout.Toggle("Natural Armor", character.naturalArmor > 0)) {
            if (character.naturalArmor == 0) {
                character.naturalArmor = 10;
            }
            character.naturalArmor =
                             EditorGUILayout.IntField("AC", character.naturalArmor);
        }
        else {
            character.naturalArmor = 0;
        }
        base.OnInspectorGUI();

        EditorGUILayout.PropertyField(customAttacksProperty, true);
        serializedObject.ApplyModifiedProperties();
        if (GUI.changed) {
            EditorUtility.SetDirty(character);
        }
    }
}
