﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

[CustomEditor(typeof(InvisibleManagedPrefab))]
[CanEditMultipleObjects]
public class InvisibleManagedPrefabEditor : ManagedPrefabEditor {

}