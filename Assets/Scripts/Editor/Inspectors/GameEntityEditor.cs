﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(GameEntity))]
[CanEditMultipleObjects]
public class GameEntityEditor : ManagedPrefabEditor {
    protected SerializedProperty isContainerProperty;
    protected SerializedProperty contentsProperty;
    protected SerializedProperty startingContentsProperty;
    protected SerializedProperty individualWeightProperty;

    protected override void OnEnable() {
        base.OnEnable();
        isContainerProperty = serializedObject.FindProperty("isContainer");
        contentsProperty = serializedObject.FindProperty("contents");
        startingContentsProperty = serializedObject.FindProperty("startingContents");
        individualWeightProperty = serializedObject.FindProperty("individualWeight");
    }

    public override void OnInspectorGUI() {
        var gameEntity = (GameEntity)target;

        EditorGUILayout.PropertyField(isContainerProperty, true);
        if (gameEntity.isContainer) {
            EditorGUILayout.PropertyField(startingContentsProperty, true);

            if (Application.isPlaying) {
                EditorGUILayout.PropertyField(contentsProperty, true);
            }
        }
        EditorGUILayout.PropertyField(individualWeightProperty, true);
        base.OnInspectorGUI();
    }

}