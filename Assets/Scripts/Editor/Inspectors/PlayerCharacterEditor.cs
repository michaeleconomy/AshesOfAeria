﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerCharacter))]
[CanEditMultipleObjects]
public class PlayerCharacterEditor : CharacterEditor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        var player = (PlayerCharacter)target;

        if (GUI.changed) {
            EditorUtility.SetDirty(player);
        }
    }
}
