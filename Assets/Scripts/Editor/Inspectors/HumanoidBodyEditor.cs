﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(HumanoidBody))]
[CanEditMultipleObjects]
public class HumanoidBodyEditor : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        var humanoidBody = (HumanoidBody)target;
        if (GUI.changed) {
            humanoidBody.RefreshAppearance();
        }
    }
}