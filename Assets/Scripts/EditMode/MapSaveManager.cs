﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class MapSaveManager : MonoBehaviour {
    public static MapSaveManager instance;

    public string currentSaveId;
    public long currentSaveTimestamp;

    public MapManager mapManager;
    public DialogData dialogData;
    public QuestManager questManager;
    public TriggerData triggerData;

    public List<S3Client.FileListItem> mapSaves;
    private const int mapSavesToKeep = 20;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        if (World.inEditMode) {
            GetMapSaves();
        }
    }

    public void GetMapSaves() {
        if (LocalSaveNewer()) {
            Load();
        }
        if (Application.isPlaying) {
            S3Client.instance.ListFiles((files) => {
                mapSaves =
                    files.OrderByDescending((arg) => arg.lastModified).ToList();
                DeleteOldMapSaves();
                CheckIfNewerSaveRemote();
            });
        }
    }

    private void DeleteOldMapSaves() {
        if (mapSaves.Count <= mapSavesToKeep) {
            return;
        }
        var excess = mapSaves.GetRange(mapSavesToKeep);
        foreach (var mapSave in excess) {
            mapSaves.Remove(mapSave);
            S3Client.instance.DeleteFile(mapSave.fileName);
        }
    }

    private void CheckIfNewerSaveRemote() {
        var mapSave = mapSaves.First();
        if (mapSave == null) {
            return;
        }
        var fileName = mapSave.fileName;
        var hyphenIndex = fileName.IndexOf('-');
        if (hyphenIndex == -1) {
            Debug.LogWarning("hyphen not found in mapsave filename");
            return;
        }
        long timestamp;
        var timestampStr = fileName.Substring(0, hyphenIndex);
        if (!long.TryParse(timestampStr, out timestamp)) {
            Debug.LogWarning("could not parse timestamp: " + timestampStr);
            return;
        }
        if (timestamp <= currentSaveTimestamp) {
            //up to date!
            return;
        }
        var confirmMsg =
            "There is a newer mapsave (" + fileName + ").  Want to load it?";
        UIDialog.Confirm(confirmMsg, () => {
            LoadFromS3(fileName);
        });
    }


    public bool LocalSaveNewer() {
        var worldSave = FileJSONer.Deserialize<WorldSave>(SaveFilePath());
        if (worldSave == null) {
            throw new Exception("save not found: " + SaveFilePath());
        }
        return currentSaveTimestamp < worldSave.timestamp;
    }

    public void Save() {
        if (!(mapManager is EditMapManager)) {
            throw new Exception("can't save - not in edit mode");
        }
        Overlay.Show("Saving...");
        var editMapManager = (EditMapManager)mapManager;
        var mapData = editMapManager.SaveBaseMaps();
        currentSaveTimestamp = EpochTime.Seconds();
        currentSaveId = currentSaveTimestamp.ToString() + "-" +
            UnityEngine.Random.Range(0, int.MaxValue);
        var worldSave = new WorldSave {
            id = currentSaveId,
            timestamp = currentSaveTimestamp,
            quests = questManager.questData.quests,
            dialogs = dialogData.dialogs,
            mapSaves = mapData.maps,
            startMap = mapData.startMap,
            triggers = triggerData.triggers
        };
        var json = JSONer.Serialize(worldSave);
        File.WriteAllText(SaveFilePath(), json);
        Debug.Log("map saved to: " + SaveFilePath());
        var s3FileName = currentSaveId + ".json";
        S3Client.instance.WriteFile(s3FileName, json, (success) => {
            if (!success) {
                UIDialog.Alert("Upload to S3 failed!", Overlay.Hide);
                return;
            }
            UIDialog.Alert("Saved File to S3 successfully", Overlay.Hide);
        });
    }


    public void LoadFromS3(string fileName) {
        Overlay.Show("Loading From S3");
        S3Client.instance.ReadFile(fileName, (file) => {
            File.WriteAllText(SaveFilePath(), file);
            Load();
            Overlay.Hide();
        });
    }

    public void Load() {
        Overlay.Show("Loading");
        var worldSave = FileJSONer.Deserialize<WorldSave>(SaveFilePath());
        if (worldSave == null) {
            Overlay.Hide();
            throw new Exception("save not found: " + SaveFilePath());
        }
        currentSaveId = worldSave.id;
        currentSaveTimestamp = worldSave.timestamp;

        dialogData.Reload(worldSave.dialogs);
        questManager.Reload(worldSave.quests);
        triggerData.triggers.Clear();
        if (worldSave.triggers != null) {
            triggerData.triggers.AddRange(worldSave.triggers);
        }

        mapManager.LoadBaseMaps(worldSave.mapSaves, worldSave.startMap);
#if UNITY_EDITOR
        EditorUtility.SetDirty(dialogData);
        EditorUtility.SetDirty(questManager.questData);
        EditorUtility.SetDirty(mapManager.mapData);
        EditorUtility.SetDirty(triggerData);
#endif
        Overlay.Hide();
    }

    private string SaveFilePath() {
        return Application.persistentDataPath + "/map_save.json";
    }

}
