﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileManager : EditObjectSource {
    public List<TileInfo> tileInfos;
    public TileBase blockingTile;

    private Dictionary<TileBase, TileInfo> tilesByTileBase;

    private void IndexByTileBase() {
        tilesByTileBase = new Dictionary<TileBase, TileInfo>();
        foreach (var info in tileInfos) {
            if (info.tileBase == null) {
                continue;
            }
            tilesByTileBase[info.tileBase] = info;
        }
    }

    public TileInfo GetInfoForTile(TileBase tileBase) {
        if (tilesByTileBase == null) {
            IndexByTileBase();
        }
        TileInfo info;
        if (!tilesByTileBase.TryGetValue(tileBase, out info)) {
            Debug.LogWarning("tilebase was not found: " + tileBase.name);
            return null;
        }
        return info;
    }

    private void Awake() {
        CleanNulls();
    }

    private void CleanNulls() {
        if (tileInfos == null) {
            return;
        }
        var count = tileInfos.RemoveAll((tileInfo) => tileInfo.tileBase == null);
        if (count > 0) {
            Debug.LogWarning("removed " + count + " null tileBases");
        }
    }

    public int GetIdForTile(TileBase tileBase) {
        if (tilesByTileBase == null) {
            IndexByTileBase();
        }
        if (tileBase == null) {
            return 0;
        }
        if (tileBase == blockingTile) {
            return 0;
        }
        TileInfo info;
        if(!tilesByTileBase.TryGetValue(tileBase, out info)) {
            Debug.LogWarning("tilebase was not found: " + tileBase.name);
            return 0;
        }
        return info.id;
    }

    public override IEnumerable<EditObjectInfo> GetAllObjects() {
        return tileInfos.Cast<EditObjectInfo>();
    }


    public TileBase GetTileBaseById(int id) {
        if (id == 0) {
            return null;
        }
        var info = (TileInfo)GetInfoById(id);
        if (info == null) {
            Debug.LogWarning("tile could not be found: " + id);
            return null;
        }
        return info.tileBase;
    }


#if UNITY_EDITOR
    public static readonly string inputDirectoryPath = "Assets/GameObjects";
    public static readonly Regex layerRegex = new Regex(@"L(\d)");
    public static readonly Regex excludeRegex = new Regex(@"/_");

    public void Reload() {
        var oldIds = new Dictionary<TileBase, int>();
        var usedIds = new HashSet<int>();
        foreach (var info in tileInfos) {
            if(info.tileBase == null) {
                continue;
            }
            if (usedIds.Contains(info.id)) {
                //prevent duplicate ids from reimporting
                continue;
            }
            oldIds[info.tileBase] = info.id;
            usedIds.Add(info.id);
        }
        tileInfos.Clear();
        Debug.Log("indexing all Tiles");
        var guids =
            AssetDatabase.FindAssets("t:TileBase", new[] { inputDirectoryPath });
        Debug.Log("found " + guids.Length + " potential tiles");

        var nextId = 1;
        foreach (var guid in guids) {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            if (excludeRegex.IsMatch(path)) {
                continue;
            }
            var tileBase = AssetDatabase.LoadAssetAtPath<TileBase>(path);
            if (tileBase == null) {
                Debug.LogWarning("no tilebase found at: " + path);
                continue;
            }
            int id;
            if (!oldIds.TryGetValue(tileBase, out id)) {
                while(usedIds.Contains(nextId)) {
                    nextId++;
                }
                id = nextId;
                nextId++;
            }

            tileInfos.Add(new TileInfo {
                tileBase = tileBase,
                layer = DetermineLayer(path),
                id = id,
                path = CleanPath(path)
            });
        }
        EditorUtility.SetDirty(this);
        Debug.Log("completed indexing all Tiles");
    }

    private string CleanPath(string path) {
        return layerRegex.Replace(path, "").
                         Replace(inputDirectoryPath, "").
                         Substring(1).
                         Replace(".asset", "");
    }

    private static int DetermineLayer(string path) {
        var matches = layerRegex.Matches(path);
        if (matches.Count == 0) {
            Debug.LogWarning("No layer designation (L\\d) found in path: " + path);
            return 0;
        }
        var lastMatch = matches[matches.Count - 1];
        var digit = lastMatch.Groups[1].Value;
        return int.Parse(digit);
    }

    public static string managerAssetPath = "Assets/Tiles/TileManager.asset";
    public static void Regenerate() {
        var tileManager =
            AssetDatabase.LoadAssetAtPath<TileManager>(managerAssetPath);
        if (tileManager == null) {
            tileManager = CreateInstance<TileManager>();
            tileManager.tileInfos = new List<TileInfo>();
            AssetDatabase.CreateAsset(tileManager, managerAssetPath);
        }
        Undo.RecordObject(tileManager, "regenerated tilemanager");
        tileManager.Reload();
        EditorUtility.SetDirty(tileManager);
    }
#endif
}
