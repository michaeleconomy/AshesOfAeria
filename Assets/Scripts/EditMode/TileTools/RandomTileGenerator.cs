﻿#if UNITY_EDITOR
using System;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.IO;
using System.Collections.Generic;

public static class RandomTileGenerator {
    public static string inputDirectoryPath = "Assets/Tiles/Base";
    public static Regex ruleFolderRegex = new Regex(@"_Random/([\w\s]+)");

    public static void GenerateAll() {
        var directories = Directory.GetDirectories(inputDirectoryPath,
                                                   "*",
                                                   SearchOption.AllDirectories);
        foreach (var directory in directories) {
            if (!ruleFolderRegex.IsMatch(directory)) {
                continue;
            }
            Generate(directory);
        }
    }

    private static void Generate(string inputTilesDirectory) {
        Debug.Log("generating ruleTile for: " + inputTilesDirectory);
        var outputPath = GetOutputPath(inputTilesDirectory);

        var existing =
            AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(outputPath);
        if (existing != null) {
            Debug.Log("Asset already exists (" + outputPath + ") not generating");
            return;
        }
        var tilesGuids =
            AssetDatabase.FindAssets("t:Tile", new[] { inputTilesDirectory });
        var randomTile = ScriptableObject.CreateInstance<RandomTile>();
        randomTile.m_Sprites = new Sprite[tilesGuids.Length];
        for (var i = 0; i < tilesGuids.Length; i++) {
            var guid = tilesGuids[i];
            var path = AssetDatabase.GUIDToAssetPath(guid);
            var tile = AssetDatabase.LoadAssetAtPath<Tile>(path);

            randomTile.m_Sprites[i] = tile.sprite;
        }
        AssetDatabase.CreateAsset(randomTile, outputPath);
    }

    private static string GetOutputPath(string inputTilesDirectory) {
        var match = ruleFolderRegex.Match(inputTilesDirectory);
        var name = match.Groups[1].Value;
        return ruleFolderRegex.Replace(inputTilesDirectory, name + ".asset");
    }
}
#endif