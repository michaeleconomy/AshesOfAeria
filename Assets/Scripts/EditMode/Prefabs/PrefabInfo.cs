﻿using System;

[Serializable]
public class PrefabInfo : EditObjectInfo {
    public ManagedPrefab managedPrefab;
}
