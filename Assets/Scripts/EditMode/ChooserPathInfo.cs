﻿using System;
using System.Collections.Generic;

public struct ChooserPathInfo {
    public IEnumerable<EditObjectInfo> infos;
    public IEnumerable<string> subDirectories;
}
