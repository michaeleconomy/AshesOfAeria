﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EditObjectPalette : MonoBehaviour {
    public static EditObjectPalette instance;

    public EditObjectInfo selectedObject;
    public readonly List<EditObjectInfo> recentObjects = new List<EditObjectInfo>();
    public int recentObjectsMax = 100;

    public IEditObjectPaletteDelegate _delegate;

    public class EditObjectPaletteSave {
        public string selectedObjectPath;
        public List<string> recentObjectPaths;
    }

    private void Awake() {
        instance = this;
    }

    private void Start() {
        var save = FileJSONer.Deserialize<EditObjectPaletteSave>(SaveFilePath());

        if (save == null) {
            selectedObject = EditObjectManager.instance.DefaultInfo();
            return;
        }
        selectedObject = EditObjectManager.instance.GetByPath(save.selectedObjectPath);
        if (selectedObject == null) {
            selectedObject = EditObjectManager.instance.DefaultInfo();
        }

        foreach (var path in save.recentObjectPaths) {
            var info = EditObjectManager.instance.GetByPath(path);
            if (info != null) {
                recentObjects.Add(info);
            }
        }
        NotifyDelegate();
    }


    public void SelectObject(EditObjectInfo o) {
        if (o == selectedObject) {
            return; //already selected
        }

        if (selectedObject != null) {
            recentObjects.Remove(o);
            recentObjects.Insert(0, selectedObject);
        }
        var extrasToTrim = recentObjects.Count - recentObjectsMax;
        if (extrasToTrim > 0) {
            recentObjects.RemoveRange(recentObjectsMax, extrasToTrim);
        }
        selectedObject = o;

        NotifyDelegate();
        Save();
    }

    public static EditObjectInfo SelectedObject() {
        return instance.selectedObject;
    }

    private void Save() {
        FileJSONer.Serialize(new EditObjectPaletteSave {
            selectedObjectPath = selectedObject.path,
            recentObjectPaths = recentObjects.Select((arg) => arg.path).ToList()
        }, SaveFilePath());
    }

    private void NotifyDelegate() {
        if (_delegate != null) {
            _delegate.ObjectsUpdated(selectedObject, recentObjects);
        }
    }

    private static string SaveFilePath() {
        return Application.persistentDataPath + "/edit_object_palette.json";
    }
}
