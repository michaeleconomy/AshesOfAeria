﻿using System;
using System.Collections.Generic;

public interface IMapEditorToolDelegate {
    void ToolSelected(MapTool tool);
}
