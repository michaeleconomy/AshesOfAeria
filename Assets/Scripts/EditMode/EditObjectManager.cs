﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EditObjectManager : MonoBehaviour {
    public static EditObjectManager instance;

    public List<EditObjectSource> sources = new List<EditObjectSource>();

    private void Awake() {
        instance = this;
    }

    public EditObjectInfo GetByPath(string path) {
        foreach (var source in sources) {
            var info = source.GetInfoByPath(path);
            if (info != null) {
                return info;
            }
        }
        return null;
    }

    public ChooserPathInfo GetAllForPath(string path) {
        var subDirectories = new HashSet<string>();
        var infos = new List<EditObjectInfo>();
        foreach (var source in sources) {
            var pathInfo = source.GetAllForPath(path);
            foreach (var sub in pathInfo.subDirectories) {
                subDirectories.Add(sub);
            }
            infos.AddRange(pathInfo.infos);
        }

        return new ChooserPathInfo {
            subDirectories = subDirectories,
            infos = infos
        };
    }

    internal EditObjectInfo DefaultInfo() {
        foreach (var x in sources[0].GetAllObjects()) {
            return x;
        }
        return null;
    }
}
