﻿using System;
using UnityEngine;

public class EraseTool : MapTool {
    private int eraseLayer = -1;
    private Vector2Int? lastDragSpot;

    public override void UseTool(Vector2 position, ManagedPrefab managedPrefab) {
        var positionInt = Vector2Int.FloorToInt(position);
        MapEditor.instance.RemoveTopTile(positionInt);
    }

    public override void StartToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        var positionInt = Vector2Int.FloorToInt(position);
        eraseLayer = MapEditor.instance.GetTopTileLayer(positionInt);
    }

    public override void ToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        if (eraseLayer < 0) {
            return;
        }

        var roundedPosition = Vector2Int.FloorToInt(position);
        if (lastDragSpot == roundedPosition) {
            return;
        }
        if (lastDragSpot == null) {
            MapEditor.instance.RemoveTileOnLayer(roundedPosition, eraseLayer);
            lastDragSpot = roundedPosition;
        }

        var distance = roundedPosition - lastDragSpot.Value;
        var steps = Math.Max(Math.Abs(distance.x), Math.Abs(distance.y));
        for (var i = 0; i < steps; i++) {
            var x = lastDragSpot.Value.x + (i * distance.x / steps);
            var y = lastDragSpot.Value.y + (i * distance.y / steps);
            MapEditor.instance.RemoveTileOnLayer(new Vector2Int(x, y), eraseLayer);
        }
        lastDragSpot = roundedPosition;
    }


    public override void EndToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        lastDragSpot = null;
    }
}
