﻿using System;
using UnityEngine;

public class BrushTool : MapTool {
    private Vector2Int? lastDragSpot;

    public override void UseTool(Vector2 position, ManagedPrefab managedPrefab) {
        var selectedObject = EditObjectPalette.SelectedObject();
        if (selectedObject is TileInfo) {
            var roundedPosition = Vector2Int.FloorToInt(position);
            var tileInfo = (TileInfo)selectedObject;
            MapEditor.instance.SetTile(roundedPosition, tileInfo);
            return;
        }
        if (selectedObject is PrefabInfo) {
            if (managedPrefab != null) {
                if (managedPrefab.prefabId == selectedObject.id) {
                    return;
                }
            }
            var prefabInfo = (PrefabInfo)selectedObject;
            var prefab = prefabInfo.managedPrefab;
            var name = GetNextPrefabName(prefab);
            var spawnedGameEntity =
                UnityEngine.Object.Instantiate(prefab, World.instance.transform);
            spawnedGameEntity.name = name;
            spawnedGameEntity.transform.position =
                prefab.freePlace ? position : position.RoundPosition();
            if (spawnedGameEntity.varyColor) {
                spawnedGameEntity.SetRandomColor();
            }
            if (spawnedGameEntity.randomSprite) {
                spawnedGameEntity.SetRandomSprite();
            }
            return;
        }
        throw new Exception("don't know how to paint: " + selectedObject.GetType());
    }

    private string GetNextPrefabName(ManagedPrefab prefab) {
        var allPrefabs = World.instance.GetComponentsInChildren<ManagedPrefab>();
        int existingCount = 0;
        foreach (var otherPrefab in allPrefabs) {
            if (otherPrefab.prefabId == prefab.prefabId) {
                existingCount++;
            }
        }
        if (existingCount > 0) {
            return prefab.name + existingCount;
        }
        return prefab.name;
    }

    public override void ToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        var selectedObject = EditObjectPalette.SelectedObject();
        //only do drags for tiles
        if (!(selectedObject is TileInfo)) {
            return;
        }

        var roundedPosition = Vector2Int.FloorToInt(position);
        if (lastDragSpot == roundedPosition) {
            return;
        }
        var tileInfo = (TileInfo)selectedObject;
        if (lastDragSpot == null) {
            MapEditor.instance.SetTile(roundedPosition, tileInfo);
            lastDragSpot = roundedPosition;
        }

        var distance = roundedPosition - lastDragSpot.Value;
        var steps = Math.Max(Math.Abs(distance.x), Math.Abs(distance.y));
        for (var i = 0; i < steps; i++) {
            var x = lastDragSpot.Value.x + (i * distance.x / steps);
            var y = lastDragSpot.Value.y + (i * distance.y / steps);
            MapEditor.instance.SetTile(new Vector2Int(x, y), tileInfo);
        }
        lastDragSpot = roundedPosition;
    }

    public override void EndToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        lastDragSpot = null;
    }
}
