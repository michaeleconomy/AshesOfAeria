﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MapSave {
    public string name;
    public List<MapSavePrefab> prefabs;
    public int xMin, yMin, xSize, ySize;
    public int[] tileLayout;

    public void SetBounds(BoundsInt bounds) {
        xMin = bounds.xMin;
        yMin = bounds.yMin;
        xSize = bounds.size.x;
        ySize = bounds.size.y;
    }

    public BoundsInt TileBounds() {
        return new BoundsInt(xMin, yMin, 0, xSize, ySize, 0);
    }
}
