﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WorldSave {
    public string id;
    public long timestamp;
    public List<MapSave> mapSaves;
    public string startMap;
    public List<Quest> quests;
    public List<DialogForMapSpeaker> dialogs;
    public List<Trigger> triggers;
}
