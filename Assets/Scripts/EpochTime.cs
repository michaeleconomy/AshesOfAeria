﻿using System;
public static class EpochTime {
    public static DateTime Epoch() {
        return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    }

    public static long Seconds() {
        var tspan = DateTime.Now - Epoch();
        return Convert.ToInt64(tspan.TotalSeconds);
    }

    public static long Milliseconds() {
        var tspan = DateTime.Now - Epoch();
        return Convert.ToInt64(tspan.TotalMilliseconds);
    }
}
