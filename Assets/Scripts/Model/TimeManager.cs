﻿using System;
using UnityEngine;

public class TimeManager : MonoBehaviour {
    public static TimeManager instance;
    public int currentTime; // in six second chunks (turns)

    private void Awake() {
        instance = this;
    }

    public void Advance(int amount) {
        if (amount <= 0) {
            throw new Exception("can't advance time for negative or zero");
        }
        currentTime += amount;
    }

    public void AdvanceMinutes(int amount) {
        Advance(amount * 10);
    }

    public void AdvanceHours(int amount) {
        AdvanceMinutes(amount * 60);
    }
}
