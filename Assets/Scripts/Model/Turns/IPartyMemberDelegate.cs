﻿using System;
public interface IPartyMemberDelegate {
    void PlayerAdded(PlayerCharacter p);
    void PlayerRemoved(PlayerCharacter p);
}
