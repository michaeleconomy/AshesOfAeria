﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class TurnManager : MonoBehaviour {
    public static TurnManager instance;

    public Character activeCharacter;
    public int currentInitative;
    public static bool inCombat;
    private bool wereDead;
    public readonly SortedList<Initiative, Character> initatives =
        new SortedList<Initiative, Character>();


    public readonly List<PlayerCharacter> party = new List<PlayerCharacter>();

    private readonly List<ICombatStatusDelegate> combatStatusDelegates =
        new List<ICombatStatusDelegate>();

    private readonly List<IPartyMemberDelegate> partyMembersDelegates =
        new List<IPartyMemberDelegate>();

    public IPartyDeadDelegate partyDeadDelegate;

    private void Awake() {
        instance = this;
    }

    public void StartMainLoop(bool fromSave = false) {
        StartCoroutine(MainTurnLoop(fromSave));
    }

    public void AddCombatStatusDelegate(ICombatStatusDelegate d) {
        combatStatusDelegates.Add(d);
    }

    public void AddPartyMemberDelegate(IPartyMemberDelegate partyMemberDelegate) {
        partyMembersDelegates.Add(partyMemberDelegate);
    }

    private IEnumerator MainTurnLoop(bool fromSave = false) {
        while(true) {
            while (initatives.Count == 0) {
                Debug.LogWarning("no characters are in initative!");
                yield return new WaitForSeconds(1f);
            }
            activeCharacter = initatives.Values[currentInitative];
            if (!fromSave) {
                activeCharacter.actions.RefreshActions();
            }
            fromSave = false;
            TurnIndicator.instance.TurnUpdated(activeCharacter);
            //PrintInitatives();
            yield return StartCoroutine(activeCharacter.TakeTurn());
            activeCharacter = null;
            //TODO - remove this
            yield return new WaitForSeconds(.2f);
            CheckPCsDead();
            currentInitative++;
            if (currentInitative >= initatives.Count) {
                currentInitative = 0;
                TimeManager.instance.Advance(1);
                TriggerManager.instance.CheckTriggers();
            }
        }
    }

    private void CheckPCsDead() {
        if (AnyPlayersAreConcious()) {
            wereDead = false;
            return;
        }
        if (!wereDead) {
            wereDead = true;
            RemoveNPCs();
            if (partyDeadDelegate != null) {
                partyDeadDelegate.PartyDead();
            }
        }
    }

    public void RemoveNPCs() {
        foreach (var npc in NPCs()) {
            RemoveCharacter(npc);
        }
    }

    private List<NonPlayerCharacter> NPCs() {
        var npcs = new List<NonPlayerCharacter>();
        foreach (var character in initatives.Values) {
            if (character is NonPlayerCharacter) {
                npcs.Add((NonPlayerCharacter)character);
            }
        }
        return npcs;
    }

    private bool AnyPlayersAreConcious() {
        foreach (var player in Players()) {
            if (player.Concious()) {
                return true;
            }
        }
        return false;
    }

    public List<PlayerCharacter> Players() {
        var players = new List<PlayerCharacter>();
        foreach (var character in initatives.Values) {
            if (character is PlayerCharacter) {
                players.Add((PlayerCharacter)character);
            }
        }
        return players;
    }

    //public void StopAndClear() {
    //    Stop();
    //    activeCharacter = null;
    //    initatives.Clear();
    //    foreach(var player in party) {
    //        foreach(var partyDelegate in partyMembersDelegates) {
    //            partyDelegate.PlayerRemoved(player);
    //        }
    //    }
    //    party.Clear();
    //    currentInitative = 0;
    //    inCombat = false;
    //}

    //TODO group identical NPCs at the same initative
    public void AddCharacter(Character c) {
        var initiative = c.RollInitative();
        AddInitative(initiative);
    }

    public void AddInitative(Initiative i) {
        var c = i.character;
        if (initatives.ContainsValue(c)) {
            //Debug.Log(c.name + " is already in the initative order");
            return;
        }
        initatives.Add(i, c);
        if (initatives.Count > 1 && initatives.IndexOfKey(i) <= currentInitative) {
            currentInitative += 1;
        }
        if (c is PlayerCharacter) {
            var player = (PlayerCharacter)c;
            party.Add(player);

            foreach (var partyDelegate in partyMembersDelegates) {
                partyDelegate.PlayerAdded(player);
            }
        }
        //PrintInitatives();
        //Debug.Log(c.name + " has been added to the initative order with initative score of " + i.Score());
        if (!inCombat && (c is NonPlayerCharacter) && !((NonPlayerCharacter)c).friendly) {
            inCombat = true;
            foreach (var d in combatStatusDelegates) {
                d.CombatStarted();
            }
        }
    }

    public void RemoveCharacter(Character c) {
        var index = initatives.IndexOfValue(c);
        if (index == -1) {
            Debug.LogWarning(c.name + " could not be removed from the initative order, as they were not present");
            return;
        }
        initatives.RemoveAt(index);
        if (index <= currentInitative) {
            currentInitative -= 1;
        }
        //PrintInitatives();
        if (c is PlayerCharacter) {
            var player = (PlayerCharacter)c;
            party.Remove(player);

            foreach (var partyDelegate in partyMembersDelegates) {
                partyDelegate.PlayerAdded(player);
            }
        }
        //Debug.Log(c.name + " has been removed from the initative order");
        if (inCombat) {
            CheckIfOutOfCombat();
        }
    }

    private void CheckIfOutOfCombat() {
        foreach (var c in initatives.Values) {
            if (c is NonPlayerCharacter && !((NonPlayerCharacter)c).friendly) {
                return;
            }
        }
        inCombat = false;

        foreach (var d in combatStatusDelegates) {
            d.CombatEnded();
        }
    }

    public PlayerCharacter CurrentPlayer() {
        if (activeCharacter is PlayerCharacter) {
            return (PlayerCharacter)activeCharacter;
        }
        return null;
    }

    //private void PrintInitatives(){
    //    var sb = new StringBuilder("current initative: ");
    //    sb.Append(currentInitative);
    //    sb.Append(", Initatives:");
    //    foreach (var i in initatives.Keys) {
    //        sb.Append(" " + i.Score() + "-" + i.character.name);
    //    }
    //    Debug.Log(sb.ToString());
    //}
}
