﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class QuestManager : MonoBehaviour {
    public static QuestManager instance;

    public QuestData questData;
    public List<Quest> active;
    public List<Quest> completed;
    public List<Quest> abandoned;

    public HashSet<string> objectivesCompleted = new HashSet<string>();

    public readonly Dictionary<string, Quest> questsById =
        new Dictionary<string, Quest>();

    private void Awake() {
        instance = this;
        Reindex();
    }

    private void Reindex() {
        questsById.Clear();
        foreach (var quest in questData.quests) {
            questsById[quest.id] = quest;
        }
    }

    public bool ObjectiveCompleted(string key) {
        return objectivesCompleted.Contains(key);
    }

    public void CompleteObjective(string key) {
        objectivesCompleted.Add(key);
        CheckCompleted();
    }

    public void Reload(List<Quest> quests) {
        questData.quests = quests;
        active.Clear();
        completed.Clear();
        abandoned.Clear();
        Reindex();
    }

    public void NewGame() {
        active.Clear();
        completed.Clear();
        abandoned.Clear();
    }

    public void Add(Quest q) {
        questData.quests.Add(q);
        questsById[q.id] = q;
    }

    public void Remove(string id) {
        Quest q;
        if (!questsById.TryGetValue(id, out q)) {
            Debug.LogWarning("quest not found: " + id);
            return;
        }
        questsById.Remove(id);
        questData.quests.Remove(q);
    }


    public void BeginQuest(string id) {
        Quest q;
        if (!questsById.TryGetValue(id, out q)) {
            Debug.LogError("couldn't find quest: " + id);
            return;
        }
        if (active.Contains(q)) {
            Debug.LogError("quest " + id + " has already been started");
            return;
        }
        if (completed.Contains(q)) {
            Debug.LogError("quest " + id + " has already been completed");
            return;
        }
        if (abandoned.Contains(q)) {
            Debug.LogError("quest " + id + " has already been abandoned");
            return;
        }
        active.Add(q);
        LogManager.Log("New quest has been added: " + q.title);
        CheckCompleted();
    }

    public bool HasQuest(string questId) {
        Quest q;
        if (!questsById.TryGetValue(questId, out q)) {
            Debug.LogError("Quest does not exist: " + questId);
            return false;
        }
        return active.Contains(q) || completed.Contains(q) || abandoned.Contains(q);
    }

    public bool OnQuest(string questId) {
        Quest q;
        if (!questsById.TryGetValue(questId, out q)) {
            Debug.LogWarning("Quest does not exist: " + questId);
            return false;
        }
        return active.Contains(q);
    }

    public bool CompletedQuest(string questId) {
        Quest q;
        if (!questsById.TryGetValue(questId, out q)) {
            Debug.LogWarning("Quest does not exist: " + questId);
            return false;
        }
        return completed.Contains(q);
    }

    public void AbandonIfActive(string questId) {
        Quest q;
        if (!questsById.TryGetValue(questId, out q)) {
            Debug.LogWarning("Quest does not exist: " + questId);
            return;
        }
        if (active.Remove(q)) {
            abandoned.Add(q);
            LogManager.Log("Quest abandoned: " + q.title);
            return;
        }
        return;
    }

    public bool OnObjective(string questId, int order) {
        Quest q;
        if (!questsById.TryGetValue(questId, out q)) {
            Debug.LogWarning("Quest does not exist: " + questId);
            return false;
        }
        if (!active.Contains(q)) {
            return false;
        }
        return q.CurrentObjectiveOrder() == order;
    }

    private Quest GetActiveQuest(string id) {
        foreach (var activeQuest in active) {
            if (activeQuest.id == id) {
                return activeQuest;
            }
        }
        return null;
    }

    public void CheckCompleted() {
        var newlyCompleted = active.Where((q) => q.Completed()).ToList();
        foreach (var q in newlyCompleted) {
            completed.Add(q);
            active.Remove(q);
            ExperienceManager.instance.AwardXP(q);
        }
    }

}
