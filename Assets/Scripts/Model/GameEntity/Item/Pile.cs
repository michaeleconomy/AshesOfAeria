﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class Pile : Item {
    public List<Sprite> sprites = new List<Sprite>();

    private void Reset() {
        stacks = true;
        isContainer = false;
    }

    protected override void Awake() {
        base.Awake();
        UpdateSprite();
    }

    protected override void QuantityUpdated() {
        UpdateSprite();
    }

    private Regex displayNameRegex = new Regex(@"(?<name>)_\d+");
    public override string DisplayName() {
        var match = displayNameRegex.Match(name);
        if (match.Success) {
            return match.Groups["name"].Value;
        }
        return name;
    }

    private Regex rangeRegex = new Regex(@"(?<start>\d+)-(?<end>\d+)");
    private void UpdateSprite() {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        for (var i = 0; i < sprites.Count; i++) {
            var sprite = sprites[i];
            var match = rangeRegex.Match(sprite.name);
            var start = int.Parse(match.Groups["start"].Value);
            var end = int.Parse(match.Groups["end"].Value);
            if (Quantity >= start && Quantity <= end) {
                spriteRenderer.sprite = sprite;
                return;
            }
        }
        Debug.LogWarning(name +
                         " doesn't have a sprite for quantity: " + Quantity);
    }
}
