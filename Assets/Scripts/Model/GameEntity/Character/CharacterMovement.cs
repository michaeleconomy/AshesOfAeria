﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO - difficult terrain
public class CharacterMovement : MonoBehaviour, ICombatStatusDelegate {
    public LayerMask enemiesLayerMask;
    public LayerMask collidingLayers;
    public float closeEnough = 0.05f;
    private float closeEnough1D;
    private float approachDistance;
    public float characterSpeed = 2.0f;
    public float collisionMargin = 0.05f;

    private List<Vector2> path = new List<Vector2>();

    private Rigidbody2D body;
    private BoxCollider2D characterCollider;
    private Character character;

    private bool haltMovement;
    private bool moving = false;
    private readonly int speedParameterId = Animator.StringToHash("speed");

    private void Awake() {
        enemiesLayerMask = LayerMask.GetMask("enemies");//NOTE THIS DOESN"T WORK BOTH WAYS
        body = GetComponent<Rigidbody2D>();
        character = GetComponent<Character>();
        characterCollider = GetComponent<BoxCollider2D>();
        closeEnough1D = closeEnough / 2;
        approachDistance = closeEnough * 5;
    }

    private void Start() {
        if (!World.inEditMode) {
            TurnManager.instance.AddCombatStatusDelegate(this);
        }
    }

    IEnumerator Move() {
        haltMovement = false;
        moving = true;
        while(true) {
            CheckWaypointReached();

            var nextWaypointNullible = NextWaypoint();

            var myTurn = character == TurnManager.instance.activeCharacter;
            //Debug.Log(name + " attempting to move");
            var animator = GetComponent<Animator>();
            if (!myTurn || nextWaypointNullible == null || character.actions.MovesLeft() <= 0) {
                //Debug.Log(name + " halted movement: " + myTurn + " " + nextWaypointNullible + " " + character.actions.MovesLeft());
                body.velocity = Vector2.zero;

                if (animator != null) {
                    animator.SetFloat(speedParameterId, 0f);
                }
                break;
            }

            var nextWaypoint = nextWaypointNullible.Value;

            var x = 0f;
            var y = 0f;
            var xDist = nextWaypoint.x - body.position.x;
            var yDist = nextWaypoint.y - body.position.y;
            if (xDist > closeEnough1D) {
                if (xDist > approachDistance) {
                    x = characterSpeed;
                }
                else {
                    x = 0;
                    body.position = new Vector2(nextWaypoint.x, body.position.y);
                }
            }
            else if (xDist < -closeEnough1D) {
                if (xDist < -approachDistance) {
                    x = -characterSpeed;
                }
                else {
                    x = 0;
                    body.position = new Vector2(nextWaypoint.x, body.position.y);
                }
            }
            else if (yDist > closeEnough1D) {
                if (yDist > approachDistance) {
                    y = characterSpeed;
                }
                else {
                    y = 0;
                    body.position = new Vector2(body.position.x, nextWaypoint.y);
                }
            }
            else if (yDist < -closeEnough1D) {
                if (yDist < -approachDistance) {
                    y = -characterSpeed;
                }
                else {
                    y = 0;
                    body.position = new Vector2(body.position.x, nextWaypoint.y);
                }
            }
            //TODO - diagonal movement! (note - need to also update pathfinding for this)
            body.velocity = new Vector2(x, y);
            if (animator != null) {
                animator.SetFloat(speedParameterId, characterSpeed);
            }
            yield return null;
        }
        moving = false;

    }

    public void HaltMovement() {
        haltMovement = true;
    }

    public IEnumerator HaltAndWait() {
        HaltMovement();
        while (moving) {
            yield return null;
        }
    }

    private Vector2? NextWaypoint() {
        if (path == null || path.Count == 0) {
            return null;
        }
        return path[0];
    }

    private void CheckWaypointReached() {
        var nextWaypoint = NextWaypoint();
        if (nextWaypoint == null) {
            return;
        }

        if (IsCloseEnough(nextWaypoint.Value)) {
            character.actions.UseMove();
            path.RemoveAt(0);
            if (path.Count == 0) {
                //Debug.Log(name + "has reached destination");
                multiTurnMoveComplete = true;
                return;
            }
            if (haltMovement && SpaceIsFree(nextWaypoint.Value)) {
                path.Clear();
                multiTurnMoveComplete = true;
            }
        }
    }

    private bool IsCloseEnough(Vector2 waypoint) {
        return Vector2.Distance(body.position, waypoint) < closeEnough;
    }

    public bool IsInRange(GameObject target, float range) {
        return IsInRange(body.position, target, range);
    }

    private bool IsInRange(Vector2 prospectivePosition,
                           GameObject target,
                           float range) {
        var targetCollider = target.GetComponent<Collider2D>();
        Vector2 closestPoint;
        if (targetCollider == null) {
            closestPoint = target.transform.position;
        }
        else {
            closestPoint = targetCollider.bounds.ClosestPoint(prospectivePosition);
        }
        var prospectiveBounds =
            new Bounds(prospectivePosition, characterCollider.size);
        var closestPoint2 = prospectiveBounds.ClosestPoint(closestPoint);
        var distance =
            Vector2.Distance(closestPoint, closestPoint2);
        return distance <= range + closeEnough;
    }

    //TODO account for line of sight
    public bool CanMoveInRange(GameObject target, float range) {
        return CanMoveInRange(target, range, character.actions.MovesLeft());
    }

    //TODO account for line of sight
    public bool CanRunInRange(GameObject target, float range) {
        var moves = character.actions.MovesLeft();
        if (character.actions.HasStandardAction()) {
            moves += character.moveSpeed;
        }
        return CanMoveInRange(target, range, moves);
    }

    //TODO account for line of sight
    public bool CanMoveInRange(GameObject target, float range, int movesLeft) {
        if (IsInRange(target, range)) {
            return true;
        }
        var pathToDest = FindPath(target, range, movesLeft);
        return pathToDest != null;
    }

    //TODO - in some cases line of sight may be needed -
    //       and this doesn't account for that.
    public IEnumerator MoveInRange(GameObject target, float range) {
        if (IsInRange(target, range)) {
            yield break;
        }
        //TODO 300 is not a good max path distance....
        path = FindPath(target, range, 300);

        if (path == null) {
            //Debug.Log("no path found");
            multiTurnMoveComplete = true;
            yield break;
        }
        yield return StartCoroutine(Move());
    }

    private bool multiTurnMoveComplete;
    public IEnumerator MultiTurnMoveInRange(GameObject target, float range) {
        multiTurnMoveComplete = false;
        while (!multiTurnMoveComplete) {
            yield return StartCoroutine(MoveInRange(target, range));
            if (multiTurnMoveComplete) {
                break;
            }
            if (character.actions.HasStandardAction()) {
                character.actions.TakeRunAction();
                yield return StartCoroutine(MoveInRange(target, range));
            }
            if (multiTurnMoveComplete) {
                break;
            }
            character.actions.EndTurn();
        }
    }

    private readonly List<Vector2> adjacentOffsets = new List<Vector2> {
        Vector2.up,
        Vector2.down,
        Vector2.left,
        Vector2.right
    };

    private List<Vector2> FindPath(GameObject target, float range, int maxMoves) {
        if (maxMoves > 10000) {
            throw new Exception("max moves is capped at 10000 to prevent infinite loop");
        }
        if (IsInRange(target, range)) {
            throw new Exception("Already in range!!!");
        }
        //var startedAt = Time.time;
        var startPosition = body.position.RoundPosition();
        //Debug.Log("attempting to find a path from " + startPosition + " to " + target);
        var seenPoints = new HashSet<Vector2>();
        var unprocessedPoints = new MySortedStack<NavigationPoint>();

        var minDistance = ManhattenDistance(startPosition, target.transform.position);
        var startPoint = new NavigationPoint {
            position = startPosition,
            path = new List<Vector2>(),
            score = minDistance
        };

        unprocessedPoints.Add(startPoint);

        while (true) {
            var p = unprocessedPoints.Pop();
            if (p == null) {
                break;
            }
            if (p.score > maxMoves) {
                //Debug.Log("aborting search - path could not be found in the max number of moves " + maxMoves + " (in " + (Time.time - startedAt) * 1000f + "ms).");
                break;
            }
            List<Vector2> newPath = new List<Vector2>(p.path);

            if (p.position != startPosition) {
                newPath.Add(p.position);
            }

            foreach (var offset in adjacentOffsets) {
                var adjacentPoint = p.position + offset;

                //if we've already seen this point before
                if (seenPoints.Contains(adjacentPoint)) {
                    continue;
                }
                if (!CanMoveFrom(p.position, adjacentPoint)) {
                    //Debug.Log("can't move from: " + p.position + " to " + adjacentPoint);
                    continue;
                }

                //if the point is in range - then we've reached our destination
                if (IsInRange(adjacentPoint, target, range) &&
                    SpaceIsFree(adjacentPoint)) {
                    newPath.Add(adjacentPoint);
                    //Debug.Log("found a route to the destination: " + newPath.Join());
                    return newPath;
                }
                seenPoints.Add(adjacentPoint);
                var newP = new NavigationPoint {
                    position = adjacentPoint,
                    path = newPath,
                    score = Score(adjacentPoint, startPosition, target.transform.position)
                };
                unprocessedPoints.Add(newP);
            }
        }
        //Debug.Log("Could not find a route between points" +
                  //startPosition + " and " + target);
        return null;
    }

    private bool CanMoveFrom(Vector2 start, Vector2 end) {
        var startWithOffset = start;
        var heading = end - start;
        var distance = heading.magnitude;
        var direction = heading / distance;
        var boxSize = characterCollider.size;
        //+ Vector2.one * collisionMargin;
        var rotation = 0f;
        var hit = Physics2D.BoxCast(startWithOffset,
                                    boxSize,
                                    rotation,
                                    direction,
                                    distance,
                                    collidingLayers);
        //if (hit.collider != null) {
        //    Debug.Log(name + " hit");
        //}
        return hit.collider == null;
    }

    private bool SpaceIsFree(Vector2 space) {
        var layerMask = LayerMask.GetMask("enemies", "players", "Default", "Water");
        var collisions =
            Physics2D.OverlapBoxAll(space, characterCollider.size, 0f, layerMask);
        foreach (var c in collisions) {
            if (c.gameObject == gameObject) {
                continue;
            }
            return false;
        }
        return true;
    }

    private float ManhattenDistance(Vector2 a, Vector2 b) {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
    }

    private float Score(Vector2 p, Vector2 startPosition, Vector2 destinationPosition) {
        var destDist = ManhattenDistance(destinationPosition, p);
        var startDist = ManhattenDistance(startPosition, p);

        return destDist + startDist;
    }

    public struct ReachableSpace {
        public Vector2Int position;
        public bool isAttack;
		public bool run;
    }

    struct SpaceToCheck {
        public Vector2 position;
        public int movesLeft;
		public bool usedRun;
        public bool hasStandardAction;
    }

    public IEnumerable<ReachableSpace> GetReachableSpaces(float attackRange) {
        var reachableSpaces = new Dictionary<Vector2, ReachableSpace>();
        var spacesToCheck = new List<SpaceToCheck>{
            new SpaceToCheck {
                position = body.position.RoundPosition(),
                movesLeft = character.actions.MovesLeft(),
                hasStandardAction = character.actions.HasStandardAction()
            }
        };

        while (spacesToCheck.Count > 0) {
            var spaceToCheck = spacesToCheck[0];
            spacesToCheck.RemoveAt(0);
            var hasStandardAction = spaceToCheck.hasStandardAction;
            var movesLeft = spaceToCheck.movesLeft;
            if (hasStandardAction) {
                var enemiesInRange =
                    Physics2D.OverlapCircleAll(spaceToCheck.position, attackRange, enemiesLayerMask);
                foreach (var enemyCollider in enemiesInRange) {
                    var enemyPosition = enemyCollider.transform.position.RoundPosition();
                    if (reachableSpaces.ContainsKey(enemyPosition)) {
                        continue;
                    }
                    reachableSpaces[enemyPosition] = new ReachableSpace {
                        position = Vector2Int.FloorToInt(enemyPosition),
                        isAttack = true
                    };
                }
            }

            if (movesLeft <= 0) {
                if (hasStandardAction) {
                    hasStandardAction = false;
                    movesLeft += character.moveSpeed;
                }
                else {
                    continue;
                }
            }
            movesLeft--;
            foreach (var offset in adjacentOffsets) {
                var position = spaceToCheck.position + offset;
                if (reachableSpaces.ContainsKey(position)) {
                    continue; //already did this space
                }
                if (!CanMoveFrom(spaceToCheck.position, position)) {
					continue;
				}
                if (SpaceIsFree(position)) {
                    reachableSpaces[position] = new ReachableSpace {
                        position = Vector2Int.FloorToInt(position),
                        isAttack = false,
                        run = !hasStandardAction
                    };
                }

                spacesToCheck.Add(new SpaceToCheck {
                    position = position,
                    hasStandardAction = hasStandardAction,
                    movesLeft = movesLeft
                });
            }
        }
		return reachableSpaces.Values;

    }

    void ICombatStatusDelegate.CombatStarted() {
        if (moving) {
            LogManager.Log("Movement halted - Entering combat");
        }
        HaltMovement();
    }

    void ICombatStatusDelegate.CombatEnded() {
        //nothing
    }
}

