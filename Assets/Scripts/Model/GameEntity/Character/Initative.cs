﻿using System;
public struct Initiative : IComparable {
    public Character character;
    public int roll;
    public int modifier;

    public int CompareTo(object obj)
    {
        var otherInitiative = (Initiative) obj;

        var scoreComparison = -(Score().CompareTo(otherInitiative.Score()));
        if (scoreComparison == 0) {
            var nameComparison = character.name.CompareTo(otherInitiative.character.name);
            if (nameComparison == 0) {
                return character.Id().CompareTo(otherInitiative.character.Id());
            }
            return nameComparison;
        }
        return scoreComparison;
    }

    public int Score() {
        return roll + modifier;
    }
}
