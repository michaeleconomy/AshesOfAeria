﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : Character {
    public int xp = 0;

    public static IPlayerActionDelegate playerActionDelegate;
    public static PlayerCharacter primaryPlayer;

    public IPlayerHealthDelegate healthDelegate;

    public bool stable;
    public int deathSavesSucceeded;
    public int deathSavesFailed;

    protected override void Awake() {
        base.Awake();
        if (primaryPlayer == null) {
            primaryPlayer = this;
        }
    }

    public override IEnumerator TakeTurn() {
        if (dead) {
            //TODO move the ghost
            yield break;
        }
        if (Unconcious()) {
            DoDeathSave();
            yield break;
        }
        if (this != primaryPlayer && !TurnManager.inCombat) {
            yield return CurrentPlayerMover.Move(primaryPlayer.gameObject, 1f);
        }
        else {
            playerActionDelegate.GetPlayerActions(this);
            while (!actions.DoneWithTurn()) {
                yield return null;
            }
        }
        playerActionDelegate.TurnEnd(this);
    }

    public override void ActionsUpdated() {
        playerActionDelegate.AvailibleActionsChanged(this);
    }

    protected override void OnDeath() {
        base.OnDeath();
        //TODO become a ghost
        //TODO drop a corpse
    }

    protected override void OnRevive() {
        base.OnRevive();
        //TODO take the body (if it exists) otherwise create a new one
        //remove ghost
    }

    public bool Unconcious() {
        return !dead && hp <= 0;
    }

    public bool Concious() {
        return !dead && hp > 0;
    }

    public void DoDeathSave() {
        if (!Unconcious()) {
            throw new Exception("can't call DoDeathSave() unless character is unconcious (called on " + name + ")");
        }
        var roll = Dice.Roll(1, 20);
        if (roll == 20) {
            ApplyHealing(1);
            return;
        }
        if (stable) {
            return;
        }
        if (roll >= 10) {
            DeathSaveSucceed();
            return;
        }
        if (roll == 1) { // apply a second fail
            DeathSaveFail();
        }
        DeathSaveFail();
    }

    private void DeathSaveSucceed() {
        LogManager.Log(name + " succeeded on a death save");
        deathSavesSucceeded++;
        if (deathSavesSucceeded >= 3) {
            stable = true;
        }
        NotifyHealthDelegate();
    }

    private void DeathSaveFail() {
        LogManager.Log(name + " failed a death save");
        deathSavesFailed++;
        if (deathSavesFailed >= 3) {
            dead = true;
            OnDeath();
        }
        NotifyHealthDelegate();
    }

    private void NotifyHealthDelegate() {
        if (healthDelegate != null) {
            healthDelegate.PlayerHealthChanged(this);
        }
    }

    public override void ApplyDamage(TrueDamage damage) {
        var hpWasZero = hp == 0;
        //skip the character stuff, doesn't do deathsaves
        GameEntityApplyDamage(damage);
        if (damage.Sum() > 0) {
            PlayDamageAnimation();
        }
        if (!hpWasZero && hp <= 0) {
            OnLoseConsciousness();
            stable = false;
            deathSavesFailed = 0;
            deathSavesSucceeded = 0;
        }
        if (hp < 0) {
            //Check for instant death
            if (Math.Abs(hp) >= hpMax) {
                dead = true;
                OnDeath();
            }
            else if (hpWasZero) {
                DeathSaveFail();
            }
        }
        NotifyHealthDelegate();
    }

    protected override bool ShowDead() {
        return dead || Unconcious();
    }

    private void OnLoseConsciousness() {
        UpdateAnimationDead();
        LogManager.Log(name + " has lost conciousness.");
    }


    private void OnRegainConsciousness() {
        UpdateAnimationDead();
        LogManager.Log(name + " has regained conciousness.");
    }

    public override int ApplyHealing(int amount) {
        var wasZero = hp <= 0;
        var total = base.ApplyHealing(amount);
        if (wasZero && total > 0) {
            OnRegainConsciousness();
        }
        NotifyHealthDelegate();
        return total;
    }

    public void AwardXP(int amount) {
        xp += amount;

        if (LevelForXp(xp) >= level) {
            //TODO - prompt for level up
        }
    }

    public class LevelXp {
        public int level;
        public int xp;
    }

    public static LevelXp[] levelXp = new LevelXp[] {
        new LevelXp{level = 1, xp = 0},
        new LevelXp{level = 2, xp = 300},
        new LevelXp{level = 3, xp = 900},
        new LevelXp{level = 4, xp = 2700},
        new LevelXp{level = 5, xp = 6500},
        new LevelXp{level = 6, xp = 14000},
        new LevelXp{level = 7, xp = 23000},
        new LevelXp{level = 8, xp = 34000},
        new LevelXp{level = 9, xp = 48000},
        new LevelXp{level = 10, xp = 64000},
        new LevelXp{level = 11, xp = 85000},
        new LevelXp{level = 12, xp = 100000},
        new LevelXp{level = 13, xp = 120000},
        new LevelXp{level = 14, xp = 140000},
        new LevelXp{level = 15, xp = 165000},
        new LevelXp{level = 16, xp = 195000},
        new LevelXp{level = 17, xp = 225000},
        new LevelXp{level = 18, xp = 265000},
        new LevelXp{level = 19, xp = 305000},
        new LevelXp{level = 20, xp = 99999999},
    };

    private static int LevelForXp(int _xp) {
        foreach (var level in levelXp) {
            if (_xp < level.xp) {
                return level.level - 1;
            }
        }
        return 20;
    }

    public override SavedPrefab Save() {
        return new SavedPlayerCharacter(this);
    }
}
