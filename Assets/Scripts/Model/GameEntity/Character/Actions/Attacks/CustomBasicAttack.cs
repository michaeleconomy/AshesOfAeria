﻿using System;
using System.Collections.Generic;

public class CustomBasicAttack : Attack {
    public CustomBasicAttackData attackData;

    public override int AttackModifier() {
        return attackData.attackModifier;
    }

    public override Damage Damage(bool critical) {
        var amount = DamageAmount(critical);
        if (amount < 0) {
            amount = 0;
        }
        return new Damage{
            new Damage.Part{
                type = attackData.damageType,
                amount = amount
            }
        };
    }

    private int DamageAmount(bool critical) {
        if (attackData.damageDiceCount == 0) {
            return attackData.damageModifier;
        }
        var diceCount = attackData.damageDiceCount;
        if (critical) {
            diceCount *= 2;
        }
        return Dice.Roll(diceCount, attackData.damageDiceSize) +
                   attackData.damageModifier;
    }

    public override float Range() {
        return attackData.range;
    }

    public override float AverageDamage() {
        if (attackData.damageDiceCount == 0) {
            return attackData.damageModifier;
        }
        return Dice.AverageRoll(attackData.damageDiceCount, attackData.damageDiceSize) +
                   attackData.damageModifier;
    }
}
