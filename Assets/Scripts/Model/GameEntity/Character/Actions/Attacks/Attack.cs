﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//thi defines one person's ability to do a specific kind of attack
abstract public class Attack {
    public Character attacker;
    public abstract int AttackModifier();
    public abstract float AverageDamage();

    public abstract Damage Damage(bool critical);

    public abstract float Range();

    public IEnumerator Do(GameEntity target) {
        //TODO multiattack
        attacker.actions.UseStandardAction();
        var animator = attacker.GetComponent<Animator>();
        if (animator != null) {
            animator.SetTrigger("attack");
        }
        //TODO - acccount for advantage/disadvantage (long ranged attack, etc);
        //TODO crits enhanced critial

        var attackRoll = Dice.Roll(1, 20);
        var attackResult = new AttackResult {
            attack = this,
            target = target,
            hit = IsHit(attackRoll, target.ArmorClass()),
            critical = attackRoll == 20
        };
            
        if (attackResult.hit) {
            attackResult.damage = Damage(attackResult.critical);
            attackResult.trueDamage = target.GetTrueDamage(attackResult.damage);
            AttackDelegate.instance.HandleAttack(attackResult);
            target.ApplyDamage(attackResult.trueDamage);
        }
        else {
            AttackDelegate.instance.HandleAttack(attackResult);
        }

        while (attacker.DoingNonDefaultAnimation()) {
            yield return null;
        }
    }

    private bool IsHit(int attackRoll, int ac) {
        var modifiedAttackRoll = attackRoll + AttackModifier();
        return attackRoll != 1 && modifiedAttackRoll >= ac;
    }
}
