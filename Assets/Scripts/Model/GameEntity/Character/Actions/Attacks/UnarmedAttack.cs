﻿using System;
using System.Collections.Generic;

public class UnarmedAttack : Attack {
    public override int AttackModifier() {
        return attacker.StrengthModifier(true);
    }

    public override float AverageDamage() {
        return 1 + attacker.StrengthModifier();
    }

    public override Damage Damage(bool critical) {
        var damage = 1;
        if (critical) {
            damage *= 2;
        }
        damage += attacker.StrengthModifier();
        if (damage < 0) {
            damage = 0;
        }
        return new Damage{
            new Damage.Part{
                type = DamageType.Bludgeoning,
                amount = damage
            }
        };
    }

    public override float Range() {
        return 1f;
    }
}
