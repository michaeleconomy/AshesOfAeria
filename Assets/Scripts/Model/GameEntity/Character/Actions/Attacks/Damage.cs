﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Damage : List<Damage.Part> {
    public class Part {
        public int amount;
        public DamageType type;
    }

    public Damage() : base() { }

    public Damage(Damage d) : base(d) { }

    public int Sum() {
        return this.Select((p) => p.amount).Sum();
    }
}
