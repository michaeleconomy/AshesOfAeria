﻿using System;
[Serializable]
public class CustomBasicAttackData {
    public string name;
    public int attackModifier;
    public float range;
    public int damageDiceCount;
    public int damageDiceSize;
    public int damageModifier;
    public DamageType damageType;
}
