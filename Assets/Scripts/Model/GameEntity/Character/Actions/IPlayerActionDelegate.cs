﻿using System;
//TODO - this is more of a action input, not a delegate
public interface IPlayerActionDelegate : ICharacterActionDelegate {
    void GetPlayerActions(PlayerCharacter player);
}
