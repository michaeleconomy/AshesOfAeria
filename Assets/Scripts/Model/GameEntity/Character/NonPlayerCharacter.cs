﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonPlayerCharacter : Character {
    public bool friendly = true;
    LayerMask playersLayerMask;
    public int xpValue; //amount of xp players will get for killing
    public int naturalArmor; //set to 0 for no natural armor, otherwise it is the raw AC value
    public bool trackDeath; //this character dying can be a quest objective

    public List<CustomBasicAttackData> customAttacks;

    protected override void Awake() {
        base.Awake();
        playersLayerMask = LayerMask.GetMask("players");
    }

    protected override void Reset() {
        base.Reset();
        hasDialog = true;
    }


    public override bool HasDialog() {
        return friendly;
    }

    public override IEnumerator TakeTurn() {
        if (dead) {
            yield break;
        }
        if (friendly) {
            Debug.Log(name + " - Friendly NPC - not doing anything");
        }
        else { //unfriendly
            var enemy = FindEnemyInRange();
            if (enemy) {
                //TODO- incorporate weapon ranges
                var range = 1f;
                if (movement.IsInRange(enemy.gameObject, range)) {
                    Debug.Log("enemy (" + enemy.name + ") in range - attacking");
                    yield return StartCoroutine(DefaultAttack().Do(enemy));
                }
                else {
                    Debug.Log("enemy (" + enemy.name + ") out of range - moving in");
                    //TODO can get in range to attack this turn?
                    yield return StartCoroutine(movement.MoveInRange(enemy.gameObject, range));
                }
            }
            else {
                Debug.Log(name + " cannot see any enemies, not doing anything");
            }
        }
        yield return null;
    }

    //TODO support friendly NPCs(different layers)
    private Character FindEnemyInRange() {
        var hitCollider = Physics2D.OverlapCircle(transform.position, sightRange, playersLayerMask);
        if (hitCollider == null) {
            return null;
        }

        return hitCollider.gameObject.GetComponent<Character>();
    }

    public override int ArmorClass() {
        if (naturalArmor > 0) {
            return naturalArmor;
        }
        return base.ArmorClass();
    }

    public override List<Attack> Attacks() {
        var attacks = base.Attacks();
        foreach (var attackData in customAttacks) {
            attacks.Add(new CustomBasicAttack{
                attacker = this,
                attackData = attackData
            });
        }
        return attacks;
    }

    protected override void OnDeath() {
        base.OnDeath();
        //TODO AWARD XP? - but only if the PCs did damage...
        TurnManager.instance.RemoveCharacter(this);
        //TODO 

    }

    public override SavedPrefab Save() {
        return new SavedNonPlayerCharacter(this);
    }
}
