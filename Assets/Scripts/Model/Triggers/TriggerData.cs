﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TriggerData : ScriptableObject {
    public List<Trigger> triggers;
    public const string anywhere = "anywhere";

    public IEnumerable<Trigger> GetForMap(string mapName) {
        return triggers.Where((t) => t.mapName == mapName);
    }
}
