﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceManager : MonoBehaviour {
    public static ExperienceManager instance;

    public HashSet<NonPlayerCharacter> charactersDamagedByParty =
        new HashSet<NonPlayerCharacter>();

    private void Awake() {
        instance = this;
    }

    public void DamageDone(Character attacker, Character defender) {
        if (attacker is PlayerCharacter && defender is NonPlayerCharacter) {
            charactersDamagedByParty.Add((NonPlayerCharacter)defender);
        }
    }

    public void CharacterKilled(Character character) {
        if (!(character is NonPlayerCharacter)) {
            return;
        }
        var npc = (NonPlayerCharacter)character;
        if(charactersDamagedByParty.Contains(npc)) {
            LogManager.Log("Party awarded " + npc.xpValue + "xp for slaying " + npc.DisplayName());
            AwardXP(npc.xpValue);
        }
        if (npc.trackDeath) {
            QuestManager.instance.CompleteObjective("kill-" + npc.name);
        }
    }

    public void AwardXP(Quest q) {
        LogManager.Log("Party awarded " + q.xp + "xp completing quest: " + q.title);
        AwardXP(q.xp);
    }

    private void AwardXP(int xp) {
        var players = TurnManager.instance.Players();

        foreach (var player in players) {
            player.AwardXP(xp / players.Count);
        }
    }
}
