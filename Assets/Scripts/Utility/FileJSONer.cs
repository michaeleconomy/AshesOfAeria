﻿using System;
using System.IO;

public static class FileJSONer {
    public static T Deserialize<T>(string filename) {
        try {
            var json = File.ReadAllText(filename);
            return JSONer.Deserialize<T>(json);
        }
        catch (FileNotFoundException) {
            return default(T);
        }
    }

    public static void Serialize(object o, string filename) {
        var json = JSONer.Serialize(o);
        File.WriteAllText(filename, json);
    }
}
